#!/bin/bash

if [ -z "$1" ]; then

    echo "Use this command with an argument, e.g.:" 
    echo "$0 Cfile"
    exit 1
fi

filename=$1
filenameOut=out.c
#shareDir=share
export shareDir=`frama-c -print-share-path`

echo "============================"
echo "Running frama-c..."
echo "============================"
rm $filenameOut
frama-c -pp-annot -machdep=x86_64 -e-acsl $filename  -then-on e-acsl -print -ocode $filenameOut

echo "============================"
echo "Running gcc..."
echo "============================"
rm out.exe 
gcc  -std=c99 -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin   -DE_ACSL_MACHDEP=x86_64 $filenameOut $shareDir/e-acsl/e_acsl.c $shareDir/e-acsl/memory_model/e_acsl_bittree.c $shareDir/e-acsl/memory_model/e_acsl_mmodel.c -lm -o out.exe  -lgmp

#gcc  -std=c99 -pedantic -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin   -DE_ACSL_MACHDEP=x86_64 $filenameOut $shareDir/e-acsl/e_acsl.c $shareDir/e-acsl/memory_model/e_acsl_bittree.c $shareDir/e-acsl/memory_model/e_acsl_mmodel.c -o out.exe -lm -lgmp

echo "============================"
echo "Running out.exe..."
echo "============================"

./out.exe

echo "============================"
echo "End!"
echo "============================"



#frama-c -e-acsl $filename  -then-on e-acsl -print -ocode $filenameOut

#%gcc $filenameOut share/e-acsl/e_acsl.c share/e-acsl/memory_model/e_acsl_bittree.c share/e-acsl/memory_model/e_acsl_mmodel.c


