/*
 * top.c
 *
 *  Created on: 8 July 2014
 *      Author: Kosmatov CEA LIST
 */

/* top C file to be compiled for E-ACSL */

#define __REQUIREMENT_04

#include "reportVmPeak.c"

#include "src/cabin.c"
#include "src/elevator.c"
#include "src/testing.c"
#include "src/ext_call.c"
#include "src/door.c"
#include "src/transitions.c"
#include "src/utils.c"
#include "src/main.c"

