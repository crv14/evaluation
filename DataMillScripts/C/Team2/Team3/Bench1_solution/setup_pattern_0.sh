#! /bin/sh

exec 1>run.log

exec 2>&1

echo "Running Experiment setup (setup.sh)" 

gcc -std=c99 -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin   -DE_ACSL_MACHDEP=x86_64 BenchmarkNameToReplace -lm -o run.out -lgmp
