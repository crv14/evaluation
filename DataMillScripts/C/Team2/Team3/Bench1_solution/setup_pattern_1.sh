#! /bin/sh

exec 1>run.monitor.log

exec 2>&1

echo "Running Experiment setup (setup.sh)" 

export SHARE=./e-acsl-library

gcc -std=c99 -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin   -DE_ACSL_MACHDEP=x86_64 BenchmarkNameToReplace $SHARE/e_acsl.c $SHARE/memory_model/e_acsl_bittree.c $SHARE/memory_model/e_acsl_mmodel.c -lm -o run.monitor.out -lgmp
