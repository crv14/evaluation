/* Free two different elements in a union which hold the same pointer data. */

#include <stdlib.h>
#include "annotated_libc.h"

int main(){
  union {
    int *A, *B;
  } u;
  u.A = malloc(sizeof(int)); 
  //bye(); //report VmPeak
  free(u.A);
  free(u.B);
  return 0;
}
