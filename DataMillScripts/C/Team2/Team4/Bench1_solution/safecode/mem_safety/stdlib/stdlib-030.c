/* memmove() with invalid source */

#include <string.h>

#include "annotated_libc.h"

int main(){
  char buffer[200];
  memmove(buffer, &buffer[200], 100);
  return 0;
}
