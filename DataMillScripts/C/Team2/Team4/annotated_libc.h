#include <stdlib.h>
#include <string.h>

// Simple partial specifications of some stdlib functions that are compatible
// with E-ACSL.

/*@ requires \valid((char*)ptr) ;
  @ requires \offset((char*)ptr) == 0 ; */
void annotated_free(void *ptr) {
  free(ptr);
}

/*@ requires \exists integer i, j; 0 <= i < \block_length(src)
  @                                && 0 <= j < \block_length(dest)
  @                                && (src[i] == '\0' && dest[j] == '\0'
  @                                && i + j < \block_length(dest)) ;
  @*/
char *strcat_spec(char *dest, const char *src) {
	return strcat(dest, src);
}

/*@ requires \valid(dest) ;
  @ requires \valid_read(src) ;
  @ requires \exists integer i; 0 <= i && i < \block_length(dest) && src[i] == '\0' ; */
char *strcpy_spec(char *dest, char *src) {
	return strcpy(dest, src);
}

// [from man] Therefore, the size of dest must be at least strlen(dest)+n+1.

/*@ requires \exists integer i; 0 <= i < \block_length(dest)
  @                             && (dest[i] == '\0'
  @							        && \block_length(dest) >= i + n + 1);
  @*/
char *strncat_spec(char *dest, const char *src, size_t n) {
	return strncat(dest, src, n);
}


/*@ requires \forall integer k; 0 <= k < n ==> \valid(((char*)dest)+k);
  @ requires \forall integer k; 0 <= k < n ==> \valid_read(((char*)src)+k);
  @ ensures \result == dest;
  @*/
void *memcpy_spec(void *restrict dest, const void *restrict src, size_t n) {
	return memcpy(dest, src, n);
}

/*@ requires \forall integer k; 0 <= k < n ==> \valid(((char*)dest)+k);
  @ requires \forall integer k; 0 <= k < n ==> \valid_read(((char*)src)+k);
  @ ensures \result == dest;
  @*/
void *memmove_spec(void *restrict dest,
				   const void *restrict src, size_t n) {
	return memmove(dest, src, n);
}

/*@ requires \forall integer k; 0 <= k < n ==> \valid(((char*)s)+k) ; */
void *memset_spec(void *s, int c, size_t n) {
	return memset(s, c, n);
}

/*@ requires \valid_read(s) ; */
size_t strlen_spec(const char *s) {
	return strlen(s);
}

char* getenv_wrap(const char *name) {
	char *s = getenv(name), *p = NULL;
	if (s != NULL) {
		p = (char*)malloc(strlen(s) + 1);
		strcpy(p, s);
	}
	return p;
}

#define free annotated_free
#define strcat strcat_spec
#define strcpy strcpy_spec
#define strncat strncat_spec
#define memcpy memcpy_spec
#define memmove memmove_spec
#define memset memset_spec
#define strlen strlen_spec
#define getenv getenv_wrap
