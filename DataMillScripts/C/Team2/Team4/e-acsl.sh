#!/bin/bash

FRAMAC="frama-c"
SHARE="/home/arvidj/dev/e-acsl/share/e-acsl"
ARGS=$(getopt -o r -l "rte" -n "e-acsl.sh" -- "$@");

#Bad arguments
if [ $? -ne 0 ]; then exit 1; fi

eval set -- "$ARGS";
while true; do
	case "$1" in
		-r|--rte)
			RTE="-rte -rte-verbose 0 -then"
			shift;
			;;
		--)
			shift;
			break;
			;;
	esac
done

COUT=/tmp/`basename $1`

if [[ "$1" -nt "$COUT" ]]; then
	if [[ -n $RTE ]]; then
		echo "Running Frama-C & RTE & E-ACSL on $1"
	else
		echo "Running Frama-C & E-ACSL on $1"
	fi
	#
	$FRAMAC -machdep x86_64 -add-path ~/dev/e-acsl \
		-e-acsl-share $SHARE \
		$1 \
		$RTE \
		-e-acsl -then-on e-acsl \
		-print -ocode $COUT > /dev/null
# else
# 	echo "Already compiled $1"
fi
