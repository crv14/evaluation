#!/bin/sh

#export SHARE=`frama-c -print-share-path`
export SHARE=/home/arvidj/dev/e-acsl/share/

gcc -g -rdynamic -std=c99 -pedantic -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin -o /tmp/`basename $1`.out $SHARE/e-acsl/e_acsl.c $SHARE/e-acsl/memory_model/e_acsl_bittree.c $SHARE/e-acsl/memory_model/e_acsl_mmodel.c $1 -lgmp
