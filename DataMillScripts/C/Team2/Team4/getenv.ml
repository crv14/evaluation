(* Consider that the `ensures' clause of getenv is valid. *)

let emitter =
  Emitter.create
    ~correctness:[] ~tuning:[]
    "getenv_validator" [ Emitter.Property_status ]

let main () =
  try
    let kf = Globals.Functions.find_by_name "getenv" in
    Annotations.iter_ensures
      (fun _ ensures ->
        let ppt =
          Property.ip_of_ensures
            kf
            Cil_types.Kglobal
            (Extlib.the (Cil.find_default_behavior (Annotations.funspec kf)))
            ensures
        in
        Property_status.emit emitter ~hyps:[] ppt Property_status.True)
      kf
      Cil.default_behavior_name
  with Not_found ->
    ()

let () = Db.Main.extend main
