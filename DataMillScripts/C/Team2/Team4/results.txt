-*- org -*-

* shadomem + blocnum, only runtime
[ ✓ ] safecode/core/buffer-001.c (RTE)
[ ✓ ] safecode/core/buffer-002.c (RTE)
[ ✓ ] safecode/core/buffer-003.c (RTE)
[ ✓ ] safecode/core/buffer-005.c (RTE)
[ ✓ ] safecode/core/buffer-006.c (-p) (RTE)
[ ✘ ] safecode/core/free-001.c
[ ✘ ] safecode/core/free-003.c
[ ✘ ] safecode/core/free-008.c
[ ✘ ] safecode/core/free-009.c
[ ✓ ] safecode/mem_safety/double_free/double_free-001.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-012.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-013.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-017.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-018.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-019.c (-p)
[ ! ] safecode/mem_safety/double_free/double_free-020.c (skipped)
[ ✓ ] safecode/mem_safety/double_free/double_free-021.c
[ ✓ ] safecode/mem_safety/double_free/double_free-023.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-029.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-030.c (-p)
[ ✘ ] safecode/mem_safety/stdlib/stdlib-013.c (-p)
[ ✘ ] safecode/mem_safety/stdlib/stdlib-024.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-025.c
[ ✓ ] safecode/mem_safety/stdlib/stdlib-027.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-030.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-036.c (-p)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-001.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-003.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-004.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-008.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-020.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-029.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-030.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-001.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-002.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-005.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-011.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-014.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-016.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-018.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-025.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-029.c (RTE)
use_after_free-031.c.out: /home/arvidj/dev/e-acsl/share/e-acsl/memory_model/e_acsl_bittree.c:269: __add_element: Assertion `brother->addr != ptr->ptr' failed.
[ ✘ ] safecode/mem_safety/use_after_free/use_after_free-031.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-033.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-036.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-037.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-038.c (RTE)
[ ✘ ] safecode/mem_safety/use_after_free/use_after_free-039.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-040.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-050.c (RTE)
Skipped: 1 / Success: 41 / Failed: 8
Time (total runtime of all tests, frama-c and gcc not included): 5.32

* e-acsl master
[ ✓ ] safecode/core/buffer-001.c (RTE)
[ ✓ ] safecode/core/buffer-002.c (RTE)
[ ✓ ] safecode/core/buffer-003.c (RTE)
[ ✓ ] safecode/core/buffer-005.c (RTE)
[ ✓ ] safecode/core/buffer-006.c (-p) (RTE)
free-001.c.out: /home/arvidj/dev/e-acsl/share/e-acsl/memory_model/e_acsl_bittree.c:323: __get_exact: Assertion `__root != ((void *)0)' failed.
[ ✘ ] safecode/core/free-001.c
free-003.c.out: /home/arvidj/dev/e-acsl/share/e-acsl/memory_model/e_acsl_bittree.c:323: __get_exact: Assertion `__root != ((void *)0)' failed.
[ ✘ ] safecode/core/free-003.c
free-008.c.out: /home/arvidj/dev/e-acsl/share/e-acsl/memory_model/e_acsl_bittree.c:323: __get_exact: Assertion `__root != ((void *)0)' failed.
[ ✘ ] safecode/core/free-008.c
free-009.c.out: /home/arvidj/dev/e-acsl/share/e-acsl/memory_model/e_acsl_bittree.c:323: __get_exact: Assertion `__root != ((void *)0)' failed.
[ ✘ ] safecode/core/free-009.c
[ ✓ ] safecode/mem_safety/double_free/double_free-001.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-012.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-013.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-017.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-018.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-019.c (-p)
[ ! ] safecode/mem_safety/double_free/double_free-020.c (skipped)
[ ✓ ] safecode/mem_safety/double_free/double_free-021.c
[ ✓ ] safecode/mem_safety/double_free/double_free-023.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-029.c (-p)
[ ✓ ] safecode/mem_safety/double_free/double_free-030.c (-p)
[ ✘ ] safecode/mem_safety/stdlib/stdlib-013.c (-p)
[ ✘ ] safecode/mem_safety/stdlib/stdlib-024.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-025.c
[ ✓ ] safecode/mem_safety/stdlib/stdlib-027.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-030.c (-p)
[ ✓ ] safecode/mem_safety/stdlib/stdlib-036.c (-p)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-001.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-003.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-004.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-008.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-020.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-029.c (RTE)
[ ✓ ] safecode/mem_safety/uninitialized_variable/uninitialized_variable-030.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-001.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-002.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-005.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-011.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-014.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-016.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-018.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-025.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-029.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-031.c (-p) (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-033.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-036.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-037.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-038.c (RTE)
[ ✘ ] safecode/mem_safety/use_after_free/use_after_free-039.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-040.c (RTE)
[ ✓ ] safecode/mem_safety/use_after_free/use_after_free-050.c (RTE)
Skipped: 1 / Success: 42 / Failed: 7
Time: 4.4

* e-acsl + shadow memory
