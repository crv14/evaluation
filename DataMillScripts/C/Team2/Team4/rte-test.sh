#!/bin/sh

COUT=`tempfile`.c

frama-c -machdep x86_64 \
	$1 \
	-rte -rte-verbose 0 -then \
	-e-acsl -then-on e-acsl \
	-print -ocode $COUT > /dev/null && \
	gcc.sh -h $COUT

	# -print -ocode $COUT  && \

