/* strlen() on invalid pointer */

#include <stdio.h>
#include <string.h>

#include "../../../annotated_libc.h"

int main()
{
  char buffer[10];
  buffer[6] = '\0';
  printf("%i\n", strlen(&buffer[-1]));
  return 0;
}
