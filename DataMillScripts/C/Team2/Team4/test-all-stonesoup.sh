#!/bin/bash

set -e

source colors.sh

# set -e

# LOG=`tempfile --suffix .log`

# echo "Storing output in $LOG."

trap "echo Abort; exit;" SIGINT SIGTERM

# files to skip
SKIP=`tempfile`
# echo "safecode/mem_safety/double_free/double_free-020.c" >> $SKIP #IPC

# all tests
TESTS=`tempfile`
find stonesoup-c-mc/ -maxdepth 2 -mindepth 2 -type d > $TESTS
# echo "stonesoup-c-mc/palindrome/TC_C_122_v1010/" > $TESTS

# RTE_TESTS=`comm -23 <( ls safecode/core/buffer-* safecode/mem_safety/uninitialized_variable/* safecode/mem_safety/use_after_free/* | sort ) <( sort $SKIP)`

# tests to run = (tests \ rte-tests \ skip).
RUN_TESTS=`comm -23 <( sort $TESTS ) <( sort $SKIP)`
# RUN_TESTS=`comm -23 <( echo $RUN_TESTS | tr " " "\n" ) <( echo $RTE_TESTS | tr " " "\n" )`


for f in `cat $SKIP | sort`; do
	echo -e "${BYel}[ ! ]${RCol} $f (skipped)"
	((SKIPPED++))
done

# echo $RTE_TESTS

# run e-acsl on all tests
FAIL=0
if [[ -n $RUN_TESTS ]]; then
	parallel --halt 0 ./test-stonesoup.sh ::: $RUN_TESTS
	# FAIL=((FAIL+$?))
fi
# if [[ -n $RTE_TESTS ]]; then
# 	parallel --halt 0 ./test-stonesoup.sh -r ::: $RTE_TESTS
# 	# FAIL=((FAIL+$?))
# fi
echo "Failed tests: $FAIL"
exit

# These are programs for which we cannot use the shadow memory or
# hybrid, since they use features which are not yet present (literal
# strings and offset/block_length-predicates)
declare -A GCCSH_FLAGS
GCCSH_FLAGS["safecode/core/buffer-006.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-036.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-024.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-027.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-013.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-001.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-012.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-013.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-017.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-018.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-019.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-023.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-029.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/uninitialized_variable/uninitialized_variable-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-005.c"]='-p' #litstr
GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-018.c"]='-p' #calloc
GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-031.c"]='-p' #litstr


# Fails that should not exit with a non-zero status code, these are
# programs which do not contain any bugs.
declare -A NO_FAIL
NO_FAIL['safecode/core/buffer-002.c']=1
NO_FAIL['safecode/core/buffer-006.c']=1


TIME_FILE=`tempfile`
TOT_TIME=0
SUCCESS=0
FAILED=0
SKIPPED=0
for f in `cat $TESTS | sort`; do
	if grep -qx "$f" $SKIP; then
		echo -e "${BYel}[ ! ]${RCol} $f (skipped)"
		((SKIPPED++))
	else
	fi
done

echo Skipped: $SKIPPED / Success: $SUCCESS / Failed: $FAILED
echo Time: `awk '{ SUM+=$1 } END { print SUM }' $TIME_FILE`
