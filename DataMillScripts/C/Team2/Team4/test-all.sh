#!/bin/bash

source colors.sh

# set -e

LOG=`tempfile --suffix .log`

echo "Storing output in $LOG."

trap "echo Abort; exit;" SIGINT SIGTERM

# files to skip
SKIP=`tempfile`
echo "safecode/mem_safety/double_free/double_free-020.c" >> $SKIP #IPC

# all tests
TESTS=`tempfile`
find safecode/ -iname *.[ic] > $TESTS
# find safecode/core/ -iname *.[ic] > $TESTS
# find safecode/mem_safety/double_free/ -iname *.[ic] > $TESTS
# find safecode/mem_safety/stdlib/ -iname *.[ic] > $TESTS
# find safecode/mem_safety/uninitialized_variable/ -iname *.[ic] > $TESTS
# find safecode/mem_safety/use_after_free/ -iname *.[ic] > $TESTS

RTE_TESTS=`comm -23 <( ls safecode/core/buffer-* safecode/mem_safety/uninitialized_variable/* safecode/mem_safety/use_after_free/* | sort ) <( sort $SKIP)`

# tests to run = (tests \ rte-tests \ skip).
RUN_TESTS=`comm -23 <( sort $TESTS ) <( sort $SKIP)`
RUN_TESTS=`comm -23 <( echo $RUN_TESTS | tr " " "\n" ) <( echo $RTE_TESTS | tr " " "\n" )`

# echo RTE: $RTE_TESTS
# echo NOR: $RUN_TESTS

# run e-acsl on all tests
if [[ -n $RUN_TESTS ]]; then
	parallel --halt 2 ./e-acsl.sh ::: $RUN_TESTS
fi
if [[ -n $RTE_TESTS ]]; then
	parallel --halt 2 ./e-acsl.sh -r ::: $RTE_TESTS
fi


# These are programs for which we cannot use the shadow memory or
# hybrid, since they use features which are not yet present (literal
# strings and offset/block_length-predicates)
declare -A GCCSH_FLAGS
GCCSH_FLAGS["safecode/core/buffer-006.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-036.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-024.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-027.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-013.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/stdlib/stdlib-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-001.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-012.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-013.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-017.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-018.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-019.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-023.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-029.c"]='-p'
GCCSH_FLAGS["safecode/mem_safety/double_free/double_free-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/uninitialized_variable/uninitialized_variable-030.c"]='-p'

GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-005.c"]='-p' #litstr
GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-018.c"]='-p' #calloc
GCCSH_FLAGS["safecode/mem_safety/use_after_free/use_after_free-031.c"]='-p' #litstr


# Fails that should not exit with a non-zero status code, these are
# programs which do not contain any bugs.
declare -A NO_FAIL
NO_FAIL['safecode/core/buffer-002.c']=1
NO_FAIL['safecode/core/buffer-006.c']=1


TIME_FILE=`tempfile`
TOT_TIME=0
SUCCESS=0
FAILED=0
SKIPPED=0
for f in `cat $TESTS | sort`; do
	if grep -qx "$f" $SKIP; then
		echo -e "${BYel}[ ! ]${RCol} $f (skipped)"
		((SKIPPED++))
	else
		INSTRUMENTED=/tmp/`basename $f`
		COMPILED=/tmp/`basename $f`.out

		#compile
		# if [[ $INSTRUMENTED -nt $COMPILED ]]; then
			# echo "flags " ${GCCSH_FLAGS[$f]}
			# gcc.sh -n ${GCCSH_FLAGS[$f]} $INSTRUMENTED 2>1 > /dev/null
			./gcc_master.sh $INSTRUMENTED 2>1
			# > /dev/null
		# fi

		# run
		/usr/bin/time -o $TIME_FILE -a -f "%e" \
			$COMPILED | grep -q "failing predicate"
		STATUS=$?

		# Create
		D="$f ${BPur}"
		if [[ -n "${GCCSH_FLAGS[$f]}" ]]; then
			D="$D (${GCCSH_FLAGS[$f]})"
		fi
		if grep -qx $f <( echo $RTE_TESTS | tr ' ' "\n" ); then
			D="$D (RTE)"
		fi
		D="$D ${RCol}"

		# expect fail
		if [[ $STATUS -eq 0 ]] || [[ -n ${NO_FAIL[$f]} ]]; then
			echo -e "${BGre}[ ✓ ]${RCol} $D"
			((SUCCESS++))
		else
			echo -e "${BRed}[ ✘ ]${RCol} $D"
			((FAILED++))
		fi
	fi
done

echo Skipped: $SKIPPED / Success: $SUCCESS / Failed: $FAILED
echo Time: `awk '{ SUM+=$1 } END { print SUM }' $TIME_FILE`
