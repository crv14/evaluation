#!/usr/bin/ruby

require 'net/http'
require 'nokogiri'
require 'open3'

# program with compiled annotations 
EXEC=ARGV[0]
# dir containing io-spec dirs
DIR=ARGV[1]

VERBOSE=false


# for outut
checkmark = "\u2713"
cross = "\u2718"

def colorize(text, color_code)
    "\e[#{color_code}m#{text}\e[0m"
end
def red(text); colorize(text, 31); end
def green(text); colorize(text, 32); end

# verbose put
def verb(text); puts(text) unless ! VERBOSE; end

verb 'start: ' + EXEC + ' ' + DIR

# for each IO-spec
Dir[DIR + "/io-specifications/*.xml"].each do |spec|
    verb spec
    
    f = File.open spec
    doc = Nokogiri::XML(f)
    
    args = []
    env = {}
    stdin = ''
    exp_stdout = false
    should_fail = false
    # exp_status = 0
    
    doc.css('io_parameter').each do |param|
        types = param.css('io_value val_type')
        raise 'Unsupported val_types ' + types.inner_text unless
            types.all? { |t| t.text == 'USE_TEXT_DIRECTLY'}
        
        feature = param.css('feature').text
        case feature
        when 'environment_variable'
            vals =  param.css('io_value val')
            env[vals[0].text] = vals[1].text
        when 'command_line'
            args.concat param.css('io_value val').map { |v| v.text }
        when 'return_status_o'
            if param.css('io_value val').text != 0
                should_fail = true
            end
        when 'stdout_terminal_o'
            raise 'already has stdout' unless exp_stdout == false
            exp_stdout = param.css('io_value val').text
        else
            raise 'Unsupported feature type: ' + feature 
        end
        
    end
    
    verb 'args: '
    verb args
    
    verb 'env: '
    verb env
    
    cmd = [EXEC].concat(args).join(' ')
    Open3.popen3(env, cmd) { | stdin, stdout, stderr, w |
        # verb 'return status: ' + w.value
        out = stdout.readlines.join("\n").strip
        
        success = false
        if !should_fail 
            if out == exp_stdout.strip
                success = true
                verb 'match'
            else
                success = false
                verb 'no match'
                verb out
                verb exp_stdout
            end
        else
            if out.include? 'failing predicate'
                success = true
            else
                success = false
                verb 'fails:  ' + out
            end
        end

        out = ''
        if success
            out += green('[ ' + checkmark.encode('utf-8' ) + ' ]')
        else 
            out += red('[ ' + cross.encode('utf-8' ) + ' ]')
        end
        out += ' ' + DIR + '/' + spec
        puts out
    }
    
    f.close 
end


