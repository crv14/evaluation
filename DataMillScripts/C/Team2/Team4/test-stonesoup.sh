#!/bin/bash

source colors.sh

# user defined
FRAMAC="frama-c"
SHARE="/home/arvidj/dev/e-acsl/share/e-acsl"

# internal
INPUT_DIR=$1
TIME_FILE=`tempfile`
ARGS=$(getopt -o r -l "rte" -n "e-acsl.sh" -- "$@");

ERROR=0
SKIP=0


#Bad arguments
if [ $# -ne 1 ]; then
	echo "Usage: $1 [dir containing io-specs]"
	exit 1;
fi

eval set -- "$ARGS";
while true; do
	case "$1" in
		-r|--rte) RTE="-rte -rte-verbose 0 -then" shift; ;;
		--) shift; break; ;;
	esac
done

INPUT=`find $INPUT_DIR -iname *.c -print0 | xargs -0 grep -l "int main" | head -n1`
INSTRUMENTED=/tmp/`basename $INPUT_DIR`_`basename $INPUT`
COMPILED=/tmp/`basename $INPUT_DIR`_`basename $INPUT`.out

# echo $INPUT
# echo $INSTRUMENTED
# echo $COMPILED

# if [[ 1 ]] && [[ "$INPUT" -nt "$INSTRUMENTED" ]]; then
	# if [[ -n $RTE ]]; then
	# 	echo "Running Frama-C & RTE & E-ACSL on $INPUT"
	# else
	# 	echo "Running Frama-C & E-ACSL on $INPUT"
	# fi

	if [[ ! -f getenv.ml ]]; then
		echo "no getenv..."
		exit 1
	fi

	# $FRAMAC -load-script getenv.ml -machdep x86_64 -add-path ~/dev/e-acsl \
	env=`tempfile -s .ml`
	out=`tempfile`
	cp getenv.ml $env
	$FRAMAC -load-script $env -machdep x86_64 -add-path ~/dev/e-acsl \
		-e-acsl-share $SHARE \
		$INPUT \
		$RTE \
		-e-acsl \
		-e-acsl-full-mmodel \
		-then-on e-acsl \
		-print -ocode $INSTRUMENTED > $out
	if [[ $? -ne 0 ]]; then
		echo "Error running Frama-C on: $INPUT"
		cat $out
		exit 1
	fi
# else
# 	echo "Already compiled $INPUT"
# fi

#compile
# if [[ $INSTRUMENTED -nt $COMPILED ]]; then
	# echo "flags " ${GCCSH_FLAGS[$INPUT]}
	# gcc.sh -n ${GCCSH_FLAGS[$INPUT]} $INSTRUMENTED 2>1 > /dev/null
	TMP=`tempfile`
	./gcc_master.sh $INSTRUMENTED 2> $TMP
	STATUS=$?
	if grep -q 'undefined reference to `alloca' $TMP ; then
		SKIP=1
		COMMENT="alloca bts#1834";
	fi
	if [[ $STATUS -ne 0 ]]; then
		echo "Error compiling $INPUT."
		cat $TMP
		ERROR=1
		COMMENT=$STATUS
	fi
	# > /dev/null
# fi

if [[ $SKIP -eq 0  ]] && [[ $ERROR -eq 0 ]]; then
	./test-stonesoup-io.rb $COMPILED $INPUT_DIR
else
	# Create
	D="$INPUT ${BPur}"
	if [[ -n $COMMENT ]]; then
		D="$D ($COMMENT)"
	else
		D="$D"
	fi
	D="$D ${RCol}"

	# expect fail
	if [[ $SKIP ]] ; then
		echo -e "${BYel}[ ! ]${RCol} $D"
	else
		echo -e "${BRed}[!!!]${RCol} $D"
	fi
fi
