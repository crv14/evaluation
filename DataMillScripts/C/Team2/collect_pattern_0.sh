#! /bin/sh

echo "Running Experiment collect (collect.sh)" >> run.log

results=run-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results run.log >> /dev/null 2>&1

echo $results
echo $results >> run.log
