#! /bin/sh

echo "Running Experiment collect (collect.sh)" >> run.monitor.log

results=run-monitor-`hostname`-`date +%Y%m%d_%H%M`.tar.gz
tar czf $results run.monitor.log >> /dev/null 2>&1

echo $results
echo $results >> run.monitor.log
