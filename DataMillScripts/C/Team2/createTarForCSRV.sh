#!/bin/sh

# creates tar archives ready for the CSRV 2014 submission

# creates one archive with, and one without monitoring for each C file
# present in the current directory

# requires in the current directory:
# - patterns:
#   setup_pattern_0.sh for setup.sh W/O monotoring
#   run_pattern_0.sh for run.sh W/O monotoring
#   collect_pattern_0.sh for collect.sh W/O monotoring
#   setup_pattern_1.sh for setup.sh WITH monotoring
#   run_pattern_1.sh for run.sh WITH monotoring
#   collect_pattern_1.sh for collect.sh WITH monotoring
# - file reportVmPeak.c
# - directory e-acsl-library that can be a link to `frama-c -print-share-path`/e-acsl
# - file annotated_libc.h if it is necessary in examples

echo "=============Starting the script $0...==============="

SHARE=`frama-c -print-share-path`
rm -rf e-acsl-library e-acsl
ln -s ${SHARE}/e-acsl/
mv e-acsl e-acsl-library

if [ $# -gt 0 ]; then
    tarNamePrefix=$1
    mask="*.c"
    cur_dir=`pwd`
    annotatedLibC="annotated_libc.h"

    rm -f run.log setup.sh *.nomonitor.c run.out
    rm -f run.sh collect.sh 
    echo "Creating archives WITHOUT monitoring..."
    cp run_pattern_0.sh run.sh
    cp collect_pattern_0.sh collect.sh
    for filename in `ls $cur_dir/$mask`
    do


    	filebaseExt=`basename $filename`
    	filebase=`basename $filename ".c"`
	fileNomonitor=${filebase}.nomonitor.c
    	archiveNameNoMonitoring=${tarNamePrefix}_${filebase}_0.tar.gz

	if [ "${filebaseExt}" != "reportVmPeak.c" ]; then
# add  include for reportVmPeak.c, set atexit and decomment reporting VmPeak if any 
            echo "#include\"reportVmPeak.c\"" > $fileNomonitor
	    sed -e '{s/\(main.*{\)/\1\n\nsetbye(); \/\/ set atexit function to report memory peak\n/g}' -e '{s/\/\/bye()/  bye()/g}' < ${filebaseExt} >> $fileNomonitor
	    
    	    echo "\n\n=================Creating archive WITHOUT monitoring for $filebaseExt..."
    	    sed -e {s/BenchmarkNameToReplace/$fileNomonitor/} setup_pattern_0.sh > setup.sh
    	    chmod +x setup.sh run.sh collect.sh
	    if [ -f "$annotatedLibC" ]; then	
		tar czf $archiveNameNoMonitoring $fileNomonitor run.sh setup.sh collect.sh reportVmPeak.c $annotatedLibC
	    else
    		tar czf $archiveNameNoMonitoring $fileNomonitor run.sh setup.sh collect.sh reportVmPeak.c	
	    fi
	    
    	    echo $archiveNameNoMonitoring
    	    echo "\nrunning setup.sh..."
    	    ./setup.sh
    	    echo "\nrunning run.sh..."
    	    ./run.sh
    	    echo "\nrunning collect.sh..."
    	    ./collect.sh
	fi

    	rm -f run.log setup.sh $fileNomonitor run.out
    done
    rm -f run.log setup.sh *.nomonitor.c bench.c run.out
    rm -f run.sh collect.sh 

    echo "Creating archives WITH monitoring..."
    cp run_pattern_1.sh run.sh
    cp collect_pattern_1.sh collect.sh
    for filename in `ls $cur_dir/$mask`
    do
	filebaseExt=`basename $filename`
	filebase=`basename $filename ".c"`
	fileNomonitor=${filebase}.nomonitor.c
	fileMonitor=${filebase}.monitor.c
	archiveNameWithMonitoring=${tarNamePrefix}_${filebase}_1.tar.gz

	if [ "${filebaseExt}" != "reportVmPeak.c" ]; then

# add  include for reportVmPeak.c, set atexit and decomment reporting VmPeak if any 
            echo "#include\"reportVmPeak.c\"" > $fileNomonitor
	    sed -e '{s/\(main.*{\)/\1\n\nsetbye(); \/\/ set atexit function to report memory peak\n/g}' -e '{s/\/\/bye()/  bye()/g}' < ${filebaseExt} >> $fileNomonitor
	    
	    echo "\n\n---------------Creating archive WITH monitoring for $filebase..."
            if [ "$2" = "-rte" ]; then
                frama-c -machdep x86_64 $fileNomonitor -e-acsl-prepare -rte -rte-verbose 0 -then -e-acsl -then-on e-acsl -print -ocode ${fileMonitor}
            else
	            frama-c -pp-annot -machdep x86_64 $fileNomonitor -e-acsl -then-on e-acsl -print -ocode ${fileMonitor}
            fi	    
	    sed -e {s/BenchmarkNameToReplace/${fileMonitor}/} setup_pattern_1.sh > setup.sh
     	    chmod +x setup.sh run.sh collect.sh

	    if [ -f "$annotatedLibC" ]; then	
		tar czf $archiveNameWithMonitoring ${fileMonitor} ${fileNomonitor} run.sh setup.sh collect.sh reportVmPeak.c e-acsl-library/* $annotatedLibC
	    else
		tar czf $archiveNameWithMonitoring ${fileMonitor} ${fileNomonitor} run.sh setup.sh collect.sh reportVmPeak.c e-acsl-library/*
	    fi

	    echo $archiveNameWithMonitoring
	    echo "\nrunning setup.sh..."
	    ./setup.sh
	    echo "\nrunning run.sh..."
	    ./run.sh
	    echo "\nrunning collect.sh..."
	    ./collect.sh
            rm -f ${fileMonitor} ${fileNomonitor} run.monitor.log setup.sh bench.c *.monitor.c *.monitor.out
	fi
    done
    rm -f run.monitor.log setup.sh bench.c *.nomonitor.c *.monitor.c *.monitor.out
    rm -f run.sh collect.sh     
else
    echo 'Use this command with a tar prefix argument, e.g.: createTarForCSVR.sh "t2_t1_b1" ["-rte"] with optional argument "-rte" for using RTE plugin'
fi
echo "==============End of $0 !=============="
