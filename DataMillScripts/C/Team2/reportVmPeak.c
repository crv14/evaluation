#include<stdio.h>
#include<stdlib.h>
// reports memory peak up to now
void bye(void)
{
  system("cat /proc/$PPID/status | grep VmPeak");
}
// sets atexit function to report memory peak, exits if it fails
void setbye(void){
  int i = atexit(bye);
  if (i != 0) {
    fprintf(stderr, "cannot set exit function\n");
    exit(1);
  }
}
