#! /bin/sh

exec 1>run.log

exec 2>&1

echo "Running Experiment setup (setup.sh)" 

gcc -std=c99 -pedantic -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin -o run.out BenchmarkNameToReplace -lgmp
