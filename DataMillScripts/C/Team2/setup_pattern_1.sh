#! /bin/sh

exec 1>run.monitor.log

exec 2>&1

echo "Running Experiment setup (setup.sh)" 

export SHARE=./e-acsl-library

gcc -std=c99 -pedantic -Wall -Wno-long-long -Wno-attributes -Wno-unused-but-set-variable -fno-builtin -o run.monitor.out $SHARE/e_acsl.c $SHARE/memory_model/e_acsl_bittree.c $SHARE/memory_model/e_acsl_mmodel.c BenchmarkNameToReplace -lgmp
