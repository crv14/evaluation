#include "util.h"
#include "strDecl.h"
#include "funcDecl.h"
#include "traverse.h"

namespace Instr {

extern SgProject* project;
extern VariableSymbolMap_t ClassStructVarRemap;
extern ScopeLockKeyMap SLKM;
extern VariableSymbolMap_t varRemap;
extern VariableSymbolMap_t ReverseMap;

void iterateOverNodes();
SgExpression* buildMultArgOverloadFn(SgName fn_name, SgExprListExp* parameter_list, SgType* retType, SgScopeStatement* scope, SgNode* pos, bool derived=false);
SgExpression* createStructUsingAddressOf(SgExpression* exp, SgNode* pos);
void insertExecFunctions();
void handleArgvInitialization();
void instrument(SgProject* proj);


void instr(SgPntrArrRefExp* array_ref);
void instr(SgDotExp* dot_exp);
void instr(SgArrowExp* arrow_exp);
void instr(SgCommaOpExp* cop);
void instrCond(SgBinaryOp* bop);
void instrAddSub(SgBinaryOp* bop);
void instr(SgAssignOp* aop);
void instr(SgBinaryOp* bop);


void instr(SgFunctionCallExp* fncall);
void instr(SgCallExpression* ce);


void instr(SgVarRefExp* ref);


void instr(SgPointerDerefExp* ptr_deref);
void instr(SgCastExp* cast_exp);
void instr(SgAddressOfOp* aop);
void instrOvlUnary(SgUnaryOp* uop);
void instr(SgUnaryOp* uop);


void instr(SgThisExp* te);
void instr(SgNewExp* ne);
void instr(SgDeleteExp* del);
void instr(SgSizeOfOp* sop);
void instr(SgRefExp* ref);

void instr(SgBasicBlock* bb);
void instr(SgGlobal* global);

void instr(SgScopeStatement* scope);


void instr(SgVariableDeclaration* varDecl);
void instr(SgFunctionDeclaration* fn_decl);
void instr(SgFunctionParameterList* param_list);

void instr(SgDeclarationStatement* decl);

void instr(SgReturnStmt* retstmt);

void instr(SgExpression* exp);
void instr(SgStatement* stmt);
void instr(SgNode* node);


SgExpression* handleAssignOverloadOps(SgNode* node);
SgExpression* handleCSAssignOverloadOps(SgExpression* assign_op);
SgExpression* handleCastExp(SgCastExp* cast_exp);
SgExpression* buildCreateEntryFnForArrays(SgExpression* array, SgScopeStatement* scope, SgStatement* pos);
void insertCreateEntry(SgExpression* array, SgScopeStatement* scope, SgStatement* pos);
bool existsInSLKM(SgScopeStatement* scope);
LockKeyPair findInSLKM(SgScopeStatement* scope);
SgType* getLockType();
SgType* getKeyType();
void handleSymbolTable(SgInitializedName* orig_name, SgInitializedName* str_name, bool addToMap);
SgInitializedName* createStructVariableFor(SgInitializedName* name);
void handleSymbolTable(SgVariableDeclaration* orig_decl, SgVariableDeclaration* str_decl, bool addToMap);
}
