#include "metalib_alt.h"

#ifdef NO_MANGLE
#ifdef __cplusplus
extern "C" {
#endif
#endif


#define TIMING_SUPPORT
#define GET_STATS

#ifdef TIMING_SUPPORT
clock_t start;
clock_t start_main;
clock_t end_main;
clock_t end;
#endif



uint64_t EnterAtFirst = 0;
uint64_t EnterScope = 0;
uint64_t ExitScope = 0;
uint64_t UpdateInitInfo = 0;
uint64_t CheckInitInfo = 0;
uint64_t ExecAtLast = 0;
uint64_t CreateDummyEntry = 0;
uint64_t CreateEntryDest = 0;
uint64_t CreateEntryLong = 0;
uint64_t CreateEntry = 0;
uint64_t ArrayBnd = 0;
uint64_t ArrayBndLookup = 0;
uint64_t CheckEntry = 0;
uint64_t MallocOvl = 0;
uint64_t CreateEntrySrc = 0;
uint64_t ReallocOvl = 0;
uint64_t NullChk = 0;
uint64_t FreeOvl = 0;
uint64_t FindLock = 0;
uint64_t FindKey = 0;
uint64_t CreateEntryBlank = 0;
uint64_t ValidEntry = 0;
uint64_t SpatChk = 0;
uint64_t TempChk = 0;
uint64_t RemoveEntry = 0;
uint64_t CreateEntryLock = 0;
uint64_t LowerBnd = 0;


void v_Ret_execAtFirst();
void v_Ret_EnterScope();
void v_Ret_ExitScope();
void v_Ret_update_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr);
void v_Ret_check_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr);
void v_Ret_execAtLast();
void v_Ret_create_dummy_entry_UL_Arg(unsigned long long addr);
void v_Ret_create_entry_UL_Arg_UL_Arg(unsigned long long dest,unsigned long long src);
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long addr, unsigned long long base, unsigned long size, unsigned long long lock);
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long long base,unsigned long size);
#ifdef INT_ARRAY_INDEX
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, int index);
#else
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, unsigned int index);
#endif
void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long index);
void v_Ret_check_entry_UL_Arg_UL_Arg(unsigned long long ptr, unsigned long long addr);
struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(unsigned long size);
void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long dest, unsigned long long src);
struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_realloc_overload___Pb__v__Pe___Type_Arg_Ul_Arg(struct __Pb__v__Pe___Type str, unsigned long size);
void v_Ret_null_check_UL_Arg(unsigned long long ptr);
void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input);
void _v_Ret_push_to_stack_UL_Arg(unsigned long long addr);
unsigned long long _UL_Ret_get_from_stack_i_Arg(unsigned int index);

unsigned long long _UL_Ret_pop_from_stack_i_Arg(unsigned int evict);

void v_Ret_create_entry_with_new_lock_UL_Arg_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long long ptr,unsigned long size);

void v_Ret_remove_entry_UL_Arg(unsigned long long );
#ifdef NO_MANGLE
#ifdef __cplusplus
}
#endif
#endif

