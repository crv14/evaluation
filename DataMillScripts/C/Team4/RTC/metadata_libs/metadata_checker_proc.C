#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>

void setupReadFifo();
void metadata_checker(void* m);

int main() {
	setupReadFifo();
	metadata_checker((void*)"Thread X");
	return 0;
}

