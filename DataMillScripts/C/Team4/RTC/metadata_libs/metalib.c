#include "metalib.h"

LockMgr LockDB;
MetaDataMgr TrackingDB;


uint64_t find_lock(unsigned long long base) {
	// Some number
	return 0;
}

uint64_t find_key(uint64_t lock) {
	// Some number
	return 0;
}

void create_blank_entry(unsigned long long addr) {
	TrackingDB.create_blank_entry(addr);
}

bool isValidEntry(unsigned long long addr) {
	TrackingDB.isValidEntry(addr);
}

void spatial_check(unsigned long long ptr, unsigned long long lower, unsigned long long upper) {

	#ifdef DUMMY_LOWER_BOUND
	if(lower == DUMMY_LOWER_BOUND && upper == DUMMY_UPPER_BOUND) {
		return;
	}
	#endif

	// More descriptive than just assert
	#ifdef DESCRIPTIVE_ERRORS 
	if(ptr < lower) {
		printf("Out of bounds: ptr: %llu, lower: %llu\n", ptr, lower);
		#ifdef SILENT_ERRORS
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), lower(" << lower << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else
	
	#ifdef SILENT_ERRORS
	if(ptr < lower) {
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), lower(" << lower << ")" << std::endl;
	}
	#else
	assert(ptr >= lower);
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
//	if(ptr > upper) {
	// We should only check until the upper... since the size is added to the
	// base... and comparing with ptr > upper implies that you can still write
	// while the pointer points to the upper limit... which is wrong.
	if(ptr >= upper) {
		printf("Out of bounds: ptr: %llu, upper: %llu\n", ptr, upper);
		#ifdef SILENT_ERRORS
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), upper(" << upper << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else
//	assert(ptr <= upper);

	#ifdef SILENT_ERRORS
	if(ptr >= upper) {
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), upper(" << upper << ")" << std::endl;
	}
	#else
	assert(ptr < upper);
	#endif

	#endif
}

void temporal_check(uint64_t lock, uint64_t key) {

	if(LockDB.isDummyLock(lock)) {
		return;
	}

	if(key == 0) {
		printf("key = 0; freed this pointer\n");
		assert(0);
	}

	if(LockDB[lock] != key) {
		printf("Temporal Error: Locks[%llu] = %llu, key: %llu\n", lock, LockDB[lock], key);
		assert(0);
	}
}

void remove_entry(struct __Pb__v__Pe___Type input) {

	struct MetaData md = TrackingDB.get_entry(input.addr);

	LockDB.remove_lock(md.lock);
	
	TrackingDB.remove_entry(input.addr);
}

void create_entry_with_new_lock(unsigned long long addr, unsigned long long base, unsigned long size) {
	IntPair lock_key = LockDB.insert_lock();
	TrackingDB.create_entry(addr, base, base + size, lock_key.first, lock_key.second);
}

void lower_bound_check(unsigned long long ptr, unsigned long long addr) {
	IntPair lower_upper = TrackingDB.get_bounds(addr);
	assert(lower_upper.first == ptr);	
}

void metadata_free_overload(unsigned long long ptr, unsigned long long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_free_overload\n");
	#endif
	metadata_check_entry(ptr, addr);
	// Also check if input.ptr is greater than base... in which case, free
	// won't be able to get the correct metadata... free can only be called
	// when the pointer points to the base of the allocation.
	lower_bound_check(ptr, addr);
	struct __Pb__v__Pe___Type input = {(void*)ptr, addr};
	remove_entry(input);
}

void metadata_realloc_overload_2(unsigned long long ptr, unsigned long long addr, unsigned long size) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_realloc_overload_2\n");
	#endif

	// Update the bounds
	//v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(str.addr, reinterpret_cast<unsigned long long>(str.ptr), size);
	// FIXME: Might need to zero out the previous lock, since we are taking a new lock here...
	create_entry_with_new_lock(addr, ptr, size);
}

void metadata_realloc_overload_1(unsigned long long ptr, unsigned long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_realloc_overload_1\n");
	#endif
	// Since we are "reallocating", the str.ptr should either be NULL or 
	// a valid entry.
	if((void*)ptr != NULL) {
		TrackingDB.isValidEntry(addr);
	}

	// Also check if ptr actually points to the lower bound of the allocation
	lower_bound_check(ptr, addr);
}

void metadata_create_entry_if_src_exists(unsigned long long dest, unsigned long long src) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_create_entry_if_src_exists\n");
	#endif
	if(!TrackingDB.entryExists(src)) {
		return;
	}
	TrackingDB.copy_entry(dest, src);
}



void metadata_malloc_overload(unsigned long long addr, unsigned long long base, unsigned long size) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_malloc_overload\n");
	threadx_file << "metadata_malloc_overload\n";
	#endif
	create_entry_with_new_lock(addr, base, size);
	#ifdef DEBUG	
	printf("\t\t\t\tmalloc overload\n");
	printf("output.ptr: %llu, output.addr: %llu\n", base,
													addr);
	#endif
}


void metadata_check_entry(unsigned long long ptr, unsigned long long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_check_entry\n");
	fflush(NULL);
	#endif
	struct MetaData md = TrackingDB.get_entry(addr);

	// Check if its a blank entry... that means this is possibly a NULL ptr
	// deref...
	#ifdef DESCRIPTIVE_ERRORS
	if(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0) {
		printf("check_entry: blank_entry derefed. NULL pointer deref\n");

		#ifdef SILENT_ERRORS
		error_file << "check_entry: blank_entry derefed at addr(" << addr << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0) {
		error_file << "check_entry: blank_entry derefed at addr(" << addr << ")" << std::endl;
	}
	#else
	assert(!(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0));
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
	//printf("Temporal check\n");
	#endif
	temporal_check(md.lock, md.key);
	#ifdef DESCRIPTIVE_ERRORS
	//printf("Spatial check\n");
	#endif
	spatial_check(ptr, md.L, md.H);
}

void metadata_array_bound_check_using_lookup(unsigned long long addr, unsigned long long index) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_array_bound_check_using_lookup\n");
	#endif
	// This is a pointer variable, which is being accessed using a pntr arr ref exp. 
	// So, do a temporal and spatial check
	// If its not a valid entry, then this pointer hasn't been initialized yet.
	
	TrackingDB.isValidEntry(addr);

	if(TrackingDB.isDummyBoundCheck(addr)) {
		return;
	}
	
	struct MetaData md = TrackingDB.get_entry(addr);
	
	// The check here is to see if the index is less than the size... 
	// Not really, a spatial check like we typically do...
	// The index will give ptr + index*sizeof(type). 
	// So, we simply have to check that its below the upper bound.
	assert(index < md.H);
	// It is possible for pointers using pntr arr refs to call this funciton, and use
	// negative array indices. The lower bound check handles that case.
	assert(index >= md.L);
	temporal_check(md.lock, md.key);
}


#ifdef INT_ARRAY_INDEX
void metadata_array_bound_check(unsigned int size, int index) {
#else
void metadata_array_bound_check(unsigned int size, unsigned int index) {
#endif
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_array_bound_check\n");
	#endif
	// FIXME: Not checking if casting can still cause the read/write to exceed
	// the bounds.
	#ifdef DESCRIPTIVE_ERRORS
	if(index >= size) {
		printf("Out of bounds. index: %u, size: %u", index, size);

		#ifdef SILENT_ERRORS
		error_file << "array_bound_check: out of bounds: index(" << index << "), size(" << size << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(index >= size) {
		error_file << "array_bound_check: out of bounds: index(" << index << "), size(" << size << std::endl;
	}
	#else
	// index shouldn't be less than zero
	assert(index >= 0);
	assert(index < size);
	#endif

	#endif


	#if 0
	MetaData md = TrackingDB[addr];
	spatial_check(index, md.L, md.H);
	#endif
}




void metadata_create_entry_3(unsigned long long addr, unsigned long long base, unsigned long size) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_create_entry_3\n");
	#endif
	// Pluck the lock and key from the top of scope stack...
	// Or, it would be passed in... this would eventually happen
	// since we would need a lock and key for each scope, or maybe even
	// each variable
	IntPair lock_key = LockDB.getTopOfSLK();
	TrackingDB.create_entry(addr, base, base + size, lock_key.first, lock_key.second);
		
	struct MetaData md;
	md.L = base;
	md.H = base + size;
}


void metadata_create_entry_4(unsigned long long addr, unsigned long long base, unsigned long size, unsigned long long lock) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_create_entry_4\n");
	#endif
	#ifdef DUMMY_LOCK
	if(lock == getDummyLock()) {
		TrackingDB.create_entry(addr, base, base + size, lock, getDummyKey());
	}
	else {
	#endif
		TrackingDB.create_entry(addr, base, base + size, lock, LockDB.getKey(lock));
	#ifdef DUMMY_LOCK
	}
	#endif
}

void metadata_create_entry_dest_src(unsigned long long dest, unsigned long long src) {

	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_create_entry_dest_src\n");
	printf("dest: %llu, src: %llu\n", dest, src);
	#endif
	if(src) {
		TrackingDB.isValidEntry(src);

		TrackingDB.copy_entry(dest, src);
	}
	else {
		TrackingDB.create_blank_entry(src);
	}
}

void metadata_create_dummy_entry(unsigned long long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_create_dummy_entry\n");
	#endif
	TrackingDB.create_dummy_entry(addr);
}

void metadata_execAtLast() {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_execAtLast\n");
	#endif
	printf("execAtLast: Begin\n");

	LockDB.checkBeforeExit();
	LockDB.removeFromSLK();
	
	printf("execAtLast: End\n");
}

void metadata_checkInitInfo(long long lock, unsigned long long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_checkInitInfo\n");
	#endif
	
}

void metadata_updateInitInfo(long long lock, unsigned long long addr) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_updateInitInfo\n");
	#endif
	//InitInfo* init_map= &InitVec[lock];
	//(*init_map)[addr] = 1;
}

void metadata_ExitScope() {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_ExitScope\n");
	#endif
	IntPair lock_key = LockDB.getTopOfSLK();
	LockDB.removeFromSLK();
}

void metadata_EnterScope() {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\tmetadata_EnterScope\n");
	#endif
	IntPair lock_key = LockDB.insertIntoSLK();
}

void metadata_execAtFirst() {
	printf("execAtFirst: Begin\n");

	#ifdef SILENT_ERRORS
	error_file.open("error.txt");
	#endif

	// Now insert a single lock into the ScopeLocksAndKeys.
	// This will be the scope stack start for the main function --
	// if this function is called from main -- and it'll be the default
	// one when the stack level checks are not implemented.
	IntPair inserted = LockDB.insertIntoSLK();

	printf("execAtFirst: End\n");
}
