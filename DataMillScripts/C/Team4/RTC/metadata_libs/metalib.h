#include "TrackingDB.h"

uint64_t FindLock = 0;
uint64_t FindKey = 0;
uint64_t CreateEntryBlank = 0;
uint64_t ValidEntry = 0;
uint64_t SpatChk = 0;
uint64_t TempChk = 0;
uint64_t RemoveEntry = 0;
uint64_t CreateEntryLock = 0;
uint64_t LowerBnd = 0;

uint64_t find_lock(unsigned long long base);
uint64_t find_key(uint64_t lock);
void create_blank_entry(unsigned long long addr);
bool isValidEntry(unsigned long long addr);
void spatial_check(unsigned long long ptr, unsigned long long lower, unsigned long long upper);
void temporal_check(uint64_t lock, uint64_t key);
void remove_entry(struct __Pb__v__Pe___Type input);
void create_entry_with_new_lock(unsigned long long addr, unsigned long long base, unsigned long size);
void lower_bound_check(unsigned long long ptr, unsigned long long addr);
void metadata_free_overload(unsigned long long ptr, unsigned long long addr);
void metadata_realloc_overload_2(unsigned long long ptr, unsigned long long addr, unsigned long size);
void metadata_realloc_overload_1(unsigned long long ptr, unsigned long addr);
void metadata_create_entry_if_src_exists(unsigned long long dest, unsigned long long src);
void metadata_malloc_overload(unsigned long long addr, unsigned long long base, unsigned long size);
void metadata_check_entry(unsigned long long ptr, unsigned long long addr);
void metadata_array_bound_check_using_lookup(unsigned long long addr, unsigned long long index);
#ifdef INT_ARRAY_INDEX
void metadata_array_bound_check(unsigned int size, int index); 
#else
void metadata_array_bound_check(unsigned int size, unsigned int index);
#endif
void metadata_create_entry_3(unsigned long long addr, unsigned long long base, unsigned long size);
void metadata_create_entry_4(unsigned long long addr, unsigned long long base, unsigned long size, unsigned long long lock);
void metadata_create_entry_dest_src(unsigned long long dest, unsigned long long src);
void metadata_create_dummy_entry(unsigned long long addr);
void metadata_execAtLast();
void metadata_checkInitInfo(long long lock, unsigned long long addr);
void metadata_updateInitInfo(long long lock, unsigned long long addr);
void metadata_ExitScope();
void metadata_EnterScope();
void metadata_execAtFirst();


