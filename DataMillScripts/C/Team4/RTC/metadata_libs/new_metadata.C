#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <math.h>
#include <map>
#include <stack>
#include <list>
#include <fstream>
#include <iostream>


#include <unistd.h>
#include <time.h>

// Making this 1 ensures that the 
// function names are not mangled
#if 1
#ifdef __cplusplus
extern "C" {
#endif
#endif

//#define DESCRIPTIVE_ERRORS

#define NUM_LOCKS 10000
//#define SILENT_ERRORS

#define TIMING_SUPPORT

struct __Pb__v__Pe___Type 
{
  void *ptr;
  unsigned long long addr;
}
;

//--------------------------------
struct MetaData {
	uint64_t L;
	uint64_t H;
	uint64_t lock;
	uint64_t key;
};


#ifdef SILENT_ERRORS
std::ofstream error_file;
#endif

typedef std::map<uint64_t, struct MetaData> MetaMap;



typedef std::pair<uint64_t*, uint64_t> PtrIntPair;
typedef std::pair<uint64_t, uint64_t> IntPair;
typedef std::stack<PtrIntPair> PtrIntPairStack;
typedef std::stack<IntPair> IntPairStack;
typedef std::list<uint64_t> IntList;
// This is the map that stores initialization information
// for each lock
typedef std::map<uint64_t, unsigned int> InitInfo;


#define GET_STATS
#define BLANK_MD

uint64_t EnterAtFirst = 0;
uint64_t EnterScope = 0;
uint64_t ExitScope = 0;
uint64_t UpdateInitInfo = 0;
uint64_t CheckInitInfo = 0;
uint64_t ExecAtLast = 0;
uint64_t FindLock = 0;
uint64_t FindKey = 0;
uint64_t CreateEntryBlank = 0;
uint64_t ValidEntry = 0;
uint64_t CreateDummyEntry = 0;
uint64_t CreateEntryDest = 0;
uint64_t CreateEntryLong = 0;
uint64_t CreateEntry = 0;
uint64_t SpatChk = 0;
uint64_t TempChk = 0;
uint64_t ArrayBnd = 0;
uint64_t ArrayBndLookup = 0;
uint64_t CheckEntry = 0;
uint64_t RemoveEntry = 0;
uint64_t CreateEntryLock = 0;
uint64_t MallocOvl = 0;
uint64_t CreateEntrySrc = 0;
uint64_t LowerBnd = 0;
uint64_t ReallocOvl = 0;
uint64_t NullChk = 0;
uint64_t FreeOvl = 0;


// The organization of TrackingDB and Locks is described here.
// TrackingDB holds the metadata for each pointer. 
// Locks holds the temporal information for each allocation/scope.
// We would need an unlimited size TrackingDB to hold all the pointers
// that currently being tracked. 
// Locks needs to be extensible as well since its tough to predict
// the exact number of dynamic locks required, statically. Allocating a 
// a huge structure might be wasteful.

// For the TrackingDB we use a hash map since we would be using
// the address of the pointer to actually find and update its metadata.
// For the Locks, we can use a structure that can dynamically increase in size
// without having to be reallocated/moved. 

// MetadataMgr is the class that holds the data and functions for the TrackingDB
// LockMgr is the class that holds the data and functions for the Locks

// In the LockMgr, we include the dynamically increasing locks part.
// This could be implemented in another subclass as well, but we'll leave it in LockMgr
// for now. Given that we support N-bit locks (N being 32-bit or 64-bit... basically the
// size of the variable used as lock index). The MSB M bits are used to select the bin
// in which we are searching for the lock. The next M bits give the second level bin
// that we should look for. Its like a tree structure, with 2^M bins at each level. We could
// skew this, instead of a uniform M-bit bin lookup, but that should be a parameter.
// Finally, the last N - 2*M bits is the size of the actual lock array. If no such array exists
// then we should make sure that N - 2*M points to the base of the array and that the current
// lookup allows for a new lock to be allocated. Once that is known, we can allocate a new lock
// array and set the lock to a given value, or zero it out depending on what is needed.


// So, lets specify the macro names for this.
#define BIN1_BITS 8
#define BIN2_BITS 8
#define BIN3_BITS 12
// BIN1_BITS + BIN2_BITS + BIN3_BITS = total number of bits being used for supporting locks
// i.e. TOTAL_BITS = BIN1_BITS + BIN2_BITS + BIN3_BITS
// max number of locks supported = 2^TOTAL_BITS
// Instead of using the "pow" function, the masks can
// be precalculated here using bit shifts.
#define BIN1_SIZE (1 << BIN1_BITS)
#define BIN2_SIZE (1 << BIN2_BITS)
#define BIN3_SIZE (1 << BIN3_BITS)
#define BIN1_MASK BIN1_SIZE - 1 
#define BIN2_MASK BIN2_SIZE - 1
#define BIN3_MASK BIN3_SIZE - 1
#define SHIFT1 BIN2_BITS + BIN3_BITS
#define SHIFT2 BIN3_BITS
// SHIFT3 is zero.
// The masks will extract the required bits from a lock index.

#define START_LOCK BIN3_SIZE
#define START_KEY 100
//#define DUMMY_LOCK 11000
#define DUMMY_LOCK 50
#define DUMMY_KEY 0
#define DUMMY_LOWER_BOUND 11000
#define DUMMY_UPPER_BOUND 11000

//#define INT_ARRAY_INDEX

// Should be in the new lock structure. Inserted here
// to compile code below with new MetaDataMgr
uint64_t getDummyLock() {
	return DUMMY_LOCK;
}

uint64_t getDummyKey() {
	return DUMMY_KEY;
}


class LockMgr {
	
	//uint64_t Locks[NUM_LOCKS];
	uint64_t*** Locks;
	uint64_t LargestUnusedKey;
	uint64_t LargestUnusedLock;
	IntPairStack ScopeLocksAndKeys;
	IntList FreeLocks;
	IntList UsedLocks;

	public:

	LockMgr() {

		// Allocate first dim
		Locks = (uint64_t***)malloc(sizeof(uint64_t**)*BIN1_SIZE);

		// Allocate second dim
		for(int first_index = 0; first_index < BIN1_SIZE; first_index++) {
			Locks[first_index] = (uint64_t**)malloc(sizeof(uint64_t*)*BIN2_SIZE);
			for(int second_index = 0; second_index < BIN2_SIZE; second_index++) {
				Locks[first_index][second_index] = NULL;
			}
		}
		
		// Allocate only the first BIN3_SIZE of entries. 
		// Rest will allocated on demand.
		// The first BIN3_SIZE will house special cases...
		// DUMMY_LOCK is in this set... These locks won't be used 
		// for actual temporal tracking
		allocate_locks(0,0); 

		// Zero out unused lock and key counters
		LargestUnusedLock = START_LOCK;
		LargestUnusedKey = START_KEY;

	}

	~LockMgr() {
		// Remove the first bin in the third dim BIN3_SIZE that is left out.
		free(Locks[0][0]);

		// Remove the second dim
		for(int first_index = 0; first_index < BIN1_SIZE; first_index++) {
			free(Locks[first_index]);
		}

		// Remove the first dim
		free(Locks);
	}

	void checkBeforeExit() {
		 assert(FreeLocks.size() == (LargestUnusedLock - START_LOCK - 1));
		assert(ScopeLocksAndKeys.size() == 1);
	}	

	uint64_t get_first_index(uint64_t lock_index) {
		// shift the lock_index by BIN2_BITS + BIN3_BITS
		uint64_t lock_index_shifted = lock_index >> SHIFT1;
		// Now mask these bits with the bin1_mask
		uint64_t first_index = lock_index_shifted & BIN1_MASK;
		return first_index;
	}

	uint64_t get_second_index(uint64_t lock_index) {
		// shift the lock_index by BIN3_BITS;
		uint64_t lock_index_shifted = lock_index >> SHIFT2;
		// Now mask these bits with the bin2_mask
		uint64_t second_index = lock_index_shifted & BIN2_MASK;
		return second_index;
	}

	uint64_t get_third_index(uint64_t lock_index) {
		// No need to shift, just mask with bin3_mask
		uint64_t third_index = lock_index & BIN3_MASK;
		return third_index;
	}

	void allocate_locks(uint64_t first_index, uint64_t second_index) {
		Locks[first_index][second_index] = (uint64_t*)malloc(sizeof(uint64_t)*BIN3_SIZE);
		memset(Locks[first_index][second_index], 0, 
						sizeof(uint64_t)*BIN3_SIZE);
	}

	uint64_t& operator[](const uint64_t &lock_index) {
		uint64_t first_index = get_first_index(lock_index);
		uint64_t second_index = get_second_index(lock_index);
		uint64_t third_index = get_third_index(lock_index);
		assert(Locks[first_index][second_index]);
		return Locks[first_index][second_index][third_index];
	}

	void allocateLockIfNecessary(uint64_t lock_index) {
		uint64_t first_index = get_first_index(lock_index);
		uint64_t second_index = get_second_index(lock_index);
		uint64_t third_index = get_third_index(lock_index);
		if(Locks[first_index][second_index]) {
			return;
		}
		else {
			assert(!third_index);
			allocate_locks(first_index, second_index);
			return;
		}
	}

	void set_key(uint64_t new_lock_index, uint64_t new_key) {
		uint64_t first_index = get_first_index(new_lock_index);
		uint64_t second_index = get_second_index(new_lock_index);
		uint64_t third_index = get_third_index(new_lock_index);
		Locks[first_index][second_index][third_index] = new_key;
		assert(Locks[first_index][second_index][third_index] == new_key);
	}

	IntPair insert_lock() {
		uint64_t new_lock_index;
		if(FreeLocks.size()) {
			new_lock_index = FreeLocks.back();
			FreeLocks.pop_back();
		}
		else {
			// Get LargestUnusedLock
		 	new_lock_index = LargestUnusedLock++;
			allocateLockIfNecessary(new_lock_index);
		}

		uint64_t new_key = LargestUnusedKey++;
		// Write the new key at the new_lock_index
		//Locks[new_lock_index] = new_key;
		set_key(new_lock_index, new_key);
		
		#ifdef DESCRIPTIVE_ERRORS
		printf("inserted: (%llu, %llu)\n", new_lock_index, new_key);
		#endif
		
		// UsedLocks will grow quite large. Might be wasteful
		#if 0
		// This will be a used lock... so enter it into the UsedLocks.
		UsedLocks.push_back(new_lock_index);
		#endif


		return std::make_pair(new_lock_index, new_key);
	}
	
	#if 0
	void removeFromUsedLocks(uint64_t val) {

		// Start searching from the back... since 
		// we always push and pop from the back
		IntList::iterator i;
		for(i = UsedLocks.begin(); i != UsedLocks.end(); i++) {
			uint64_t curr = *i;
			if(curr == val) {
				break;
			}
		}

		if(i == UsedLocks.end()) {
			printf("Can't remove %llu from UsedLocks. Not found!\n", val);
			assert(0);
		}

		UsedLocks.erase(i);

		return;
	}
	#endif

	void remove_lock(uint64_t lock_index) {
		#ifdef DUMMY_LOCK
		if(lock_index == getDummyLock()) {
			// Don't remove this lock
			return;
		}
		#endif

		#ifdef DESCRIPTIVE_ERRORS
		printf("removing: (%llu, %llu)\n", lock_index, Locks[lock_index]);
		#endif

		// Now, we zero out the lock_index location
		set_key(lock_index, 0);

		// Remove lock_index from UsedLocks
		//removeFromUsedLocks(lock_index);

		// Add it to the FreeLocks
		FreeLocks.push_back(lock_index);

		return;
	}

	IntPair getTopOfSLK() {
		#ifdef DUMMY_LOCK
		IntPair lock_key = ScopeLocksAndKeys.top();
		assert(lock_key.first != getDummyLock());
		#endif
		return ScopeLocksAndKeys.top();
	}

	long long L_Ret_getTopLock() {
		IntPair slk_top = getTopOfSLK();
		return slk_top.first;
	}

	long long L_Ret_getTopKey() {
		IntPair slk_top = getTopOfSLK();
		return slk_top.second;
	}

	uint64_t getKey(uint64_t lock_index) {
		uint64_t first_index = get_first_index(lock_index);
		uint64_t second_index = get_second_index(lock_index);
		uint64_t third_index = get_third_index(lock_index);
		return Locks[first_index][second_index][third_index];
	}

	void removeFromSLK() {

		// Get the lock that we are removing from SLK
		IntPair lock_key = getTopOfSLK();
		uint64_t lock = lock_key.first;
		uint64_t key = lock_key.second;

		// Remove the lock from the UsedLocks and push it
		// into FreeLocks. Also zero it out. All done by
		// remove_lock
		remove_lock(lock);

		// Now, remove the lock from SLK itself
		ScopeLocksAndKeys.pop();
	}

	IntPair insertIntoSLK() {

		IntPair new_lock = insert_lock();
		ScopeLocksAndKeys.push(new_lock);
		printf("insertIntoSLK:top: (%llu, %llu)\n", new_lock.first, new_lock.second);

		return new_lock;
	}

	void printList(IntList& elems) {
		printf("List:");
		for(IntList::iterator it = elems.begin();
						it != elems.end(); ++it) {
				printf("%llu,",*it);
		}
		printf("\n");
		printf("End\n");
	}
	
	bool isDummyLock(uint64_t lock_index) {
		#ifdef DUMMY_LOCK
		return lock_index == getDummyLock();
		#else
		return false;
		#endif
	}

};



//uint64_t Locks[NUM_LOCKS];
//uint64_t LargestUnusedKey;
LockMgr LockDB;


class MetaDataMgr {

	private:
	MetaMap Tracker;	

	public:
	void copy_entry(uint64_t dest, uint64_t src) {
		isValidEntry(src);
		Tracker[dest] = get_entry(src);
	}

	void create_entry(uint64_t addr, uint64_t lower, uint64_t upper, 
					  uint64_t lock, uint64_t key) {
		struct MetaData md; 
		md.L = lower; md.H = upper; md.lock = lock; md.key = key;
		set_entry(addr, md);
	}
	
	void create_blank_entry(uint64_t addr) {
		struct MetaData md;
		md.L = 0; md.H = 0; md.lock = 0; md.key = 0;
		set_entry(addr, md);	
	}

	void create_dummy_entry(uint64_t addr) {
		struct MetaData md;
		md.L = DUMMY_LOWER_BOUND; md.H = DUMMY_UPPER_BOUND;
		md.lock = getDummyLock(); md.key = getDummyKey();
		set_entry(addr, md);
	}

	void remove_entry(uint64_t addr) {
		isValidEntry(addr);
		// Zero out the entry before erasing it
		create_blank_entry(addr);
		Tracker.erase(addr);
	}

	IntPair get_bounds(uint64_t addr) {
		struct MetaData md = get_entry(addr);
		return std::make_pair(md.L, md.H);
	}

	IntPair get_lock_and_key(uint64_t addr) {
		struct MetaData md = get_entry(addr);
		return std::make_pair(md.lock, md.key);
	}

	void isValidEntry(uint64_t addr) {
		assert(Tracker.find(addr) != Tracker.end());
	}

	bool entryExists(uint64_t addr) {
		return (Tracker.find(addr) != Tracker.end());
	}

	struct MetaData get_entry(uint64_t addr) {
		return Tracker[addr];	
	}

	void set_entry(uint64_t addr, struct MetaData md) {
		Tracker[addr] = md;
	}

	void print_entry(uint64_t addr, uint64_t lower, uint64_t upper,
					uint64_t lock, uint64_t key) {
		printf("[%p]:(l)%p, (h)%p, (s)%llu, (l)%llu, (k)%llu\n", addr, lower, upper,
				upper - lower, lock, key);
	}
	
	void print_TrackingDB() {
		printf("Printing TrackingDB\n");	
		MetaMap::iterator it = Tracker.begin();
		for(; it != Tracker.end(); it++) {
			uint64_t addr = it->first;
			MetaData md = it->second;
			print_entry(addr, md.L, md.H - md.L, md.lock, md.key);
		}
		printf("Done Printing\n");
	}


	bool isDummyBoundCheck(uint64_t addr) {
		#ifdef DUMMY_LOWER_BOUND
		struct MetaData md = get_entry(addr);
		if(md.L == DUMMY_LOWER_BOUND && md.H == DUMMY_UPPER_BOUND) {
			// Skip the checks
			return true;
		}
		#endif
		return false;
	}
};

MetaDataMgr TrackingDB;

// This is the array that stores the initialization information
// for all the locks
//InitInfo InitVec[NUM_LOCKS];

//PtrIntPairStack ScopeLocksAndKeys;
//IntPairStack ScopeLocksAndKeys;
//IntList FreeLocks;
//IntList UsedLocks;

#if 0
IntPair insert_lock() {
	
	assert(FreeLocks.size() != 0);

	uint64_t new_lock_index = FreeLocks.back();
	uint64_t new_key = LargestUnusedKey++;
	FreeLocks.pop_back();

	// Write the new_key at the new_lock_index
	Locks[new_lock_index] = new_key;

	// This will be a used lock... so enter it into the UsedLocks.
	UsedLocks.push_back(new_lock_index);

	#ifdef DESCRIPTIVE_ERRORS
	printf("inserted: (%llu, %llu)\n", new_lock_index, new_key);
	#endif

	return std::make_pair(new_lock_index, new_key);
}

void removeFromUsedLocks(uint64_t val) {
	
	// Start searching from the back... since 
	// we always push and pop from the back
	IntList::iterator i;
	for(i = UsedLocks.begin(); i != UsedLocks.end(); i++) {
		uint64_t curr = *i;
		if(curr == val) {
			break;
		}
	}

	if(i == UsedLocks.end()) {
		printf("Can't remove %llu from UsedLocks. Not found!\n", val);
		assert(0);
	}

	UsedLocks.erase(i);

	return;
			
}

void remove_lock(uint64_t lock_index) {

	#ifdef DESCRIPTIVE_ERRORS
	printf("removing: (%llu, %llu)\n", lock_index, Locks[lock_index]);
	#endif

	// Now, we zero out the lock_index location
	Locks[lock_index] = 0;

	// Remove lock_index from UsedLocks
	removeFromUsedLocks(lock_index);

	// Add it to the FreeLocks
	FreeLocks.push_back(lock_index);
	
	return;
}

IntPair getTopOfSLK() {
	return ScopeLocksAndKeys.top();
}

long long L_Ret_getTopLock() {
	IntPair slk_top = getTopOfSLK();
	return slk_top.first;
}

long long L_Ret_getTopKey() {
	IntPair slk_top = getTopOfSLK();
	return slk_top.second;
}

uint64_t getKey(uint64_t lock_index) {
	return Locks[lock_index];
}

void removeFromSLK() {
	
	// Get the lock that we are removing from SLK
	IntPair lock_key = getTopOfSLK();
	uint64_t lock = lock_key.first;
	uint64_t key = lock_key.second;

	// Remove the lock from the UsedLocks and push it
	// into FreeLocks. Also zero it out. All done by
	// remove_lock
	remove_lock(lock);

	// Now, remove the lock from SLK itself
	ScopeLocksAndKeys.pop();
}

IntPair insertIntoSLK() {
	
	IntPair new_lock = insert_lock();
	ScopeLocksAndKeys.push(new_lock);
	printf("insertIntoSLK:top: (%llu, %llu)\n", new_lock.first, new_lock.second);

	return new_lock;
}

void printList(IntList& elems) {
	printf("List:");
	for(IntList::iterator it = elems.begin();
		it != elems.end(); ++it) {
		printf("%llu,",*it);
	}
	printf("\n");
	printf("End\n");
}

void print_entry(unsigned long long addr, unsigned long long base, unsigned long size,
				 uint64_t lock, uint64_t key) {

	
	printf("---------------------------------\n");
	printf("addr: %p, base: %p, size: %lu\n", addr, base, size);
	printf("lower: %llu, upper: %llu\n", base, base+size);
	printf("lock: %llu, key: %llu\n", lock, key);
	printf("---------------------------------\n");

}



void print_SLK() {
	// Since we are maintaining the SLK as a stack,
	// currently we don't implement the print function.
	// We'll implement this using a vector for SLK
	// instead of a stack... in debug mode.
	printf("SLK is stack. Printing nothing here\n");
}
#endif


#ifdef TIMING_SUPPORT
clock_t start;
clock_t start_main;
clock_t end_main;
clock_t end;
#endif

void v_Ret_execAtFirst() {

	#ifdef GET_STATS
	EnterAtFirst++;
	#endif
	#ifdef TIMING_SUPPORT
	start = clock();
	#endif
	
	printf("execAtFirst: Begin\n");

	// This work is done in the constructor
	#if 0
	LargestUnusedKey = START_KEY;
	printf("LargestUnusedKey: %llu\n", LargestUnusedKey);

	// zero out the Locks array. No lock is used to begin with.
	memset(Locks, 0, sizeof(Locks));

	// enter all the locks into FreeLocks
	// Locks numbers are same as indices
	for(unsigned int index = 0; index < NUM_LOCKS; index++) {
		FreeLocks.push_back(index);
	}
	#endif

	#ifdef SILENT_ERRORS
	error_file.open("error.txt");
	#endif
	
	// Now insert a single lock into the ScopeLocksAndKeys.
	// This will be the scope stack start for the main function --
	// if this function is called from main -- and it'll be the default
	// one when the stack level checks are not implemented.
	IntPair inserted = LockDB.insertIntoSLK();

	printf("execAtFirst: End\n");
}

void v_Ret_EnterScope() {
	#ifdef GET_STATS
	EnterScope++;
	#endif
	IntPair lock_key = LockDB.insertIntoSLK();
	//InitInfo* init_map = &InitVec[lock_key.first];
	//(*init_map).clear();
}



void v_Ret_ExitScope() {
	#ifdef GET_STATS
	ExitScope++;
	#endif
	IntPair lock_key = LockDB.getTopOfSLK();
	//InitInfo* init_map = &InitVec[lock_key.first];
	//(*init_map).clear();
	LockDB.removeFromSLK();
}

void v_Ret_update_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr) {
	#ifdef GET_STATS
	UpdateInitInfo++;
	#endif
	//InitInfo* init_map= &InitVec[lock];
	//(*init_map)[addr] = 1;
}

void v_Ret_check_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr) {
	#ifdef GET_STATS
	CheckInitInfo++;
	#endif
	#if 0
	InitInfo* init_map= &InitVec[lock];
	#ifdef SILENT_ERRORS
	if((*init_map)[addr] != 1) {
		error_file << "check_initinfo: *init_map[" << (*init_map)[addr] << "] != 1" << std::endl;
	}
	#else
	assert((*init_map)[addr] == 1);
	#endif
	#endif
}

void v_Ret_execAtLast() {
	
	#ifdef GET_STATS
	ExecAtLast++;

	printf("EnterAtFirst: %llu\n", EnterAtFirst);
	printf("EnterScope: %llu\n", EnterScope);
	printf("ExitScope: %llu\n", ExitScope);
	printf("UpdateInitInfo: %llu\n", UpdateInitInfo);
	printf("CheckInitInfo: %llu\n", CheckInitInfo);
	printf("ExecAtLast: %llu\n", ExecAtLast);
	printf("FindLock: %llu\n", FindLock);
	printf("FindKey: %llu\n", FindKey);
	printf("CreateEntryBlank: %llu\n", CreateEntryBlank);
	printf("ValidEntry: %llu\n", ValidEntry);
	printf("CreateDummyEntry: %llu\n", CreateDummyEntry);
	printf("CreateEntryDest: %llu\n", CreateEntryDest);
	printf("CrateEntryLong: %llu\n", CreateEntryLong);
	printf("CreateEntry: %llu\n", CreateEntry);
	printf("SpatChk: %llu\n", SpatChk);
	printf("TempChk: %llu\n", TempChk);
	printf("ArrayBnd: %llu\n", ArrayBnd);
	printf("ArrayBndLookup: %llu\n", ArrayBndLookup);
	printf("CheckEntry: %llu\n", CheckEntry);
	printf("RemoveEntry: %llu\n", RemoveEntry);
	printf("CreateEntryLock: %llu\n", CreateEntryLock);
	printf("MallocOvl: %llu\n", MallocOvl);
	printf("CreateEntrySrc: %llu\n", CreateEntrySrc);
	printf("LowerBnd: %llu\n", LowerBnd);
	printf("ReallocOvl: %llu\n", ReallocOvl);
	printf("NullChk: %llu\n", NullChk);
	printf("FreeOvl: %llu\n", FreeOvl);
	#endif

	printf("execAtLast: Begin\n");
	LockDB.checkBeforeExit();
	LockDB.removeFromSLK();

	#ifdef TIMING_SUPPORT
	end = clock();

    printf( "Total: %6.2f\n"
          , (((double) (end-start)) / CLOCKS_PER_SEC)
          );

	#endif



	#if 0
	// The size of the FreeLocks should be one less than than NUM_LOCKS
	// since we will still be in scope for the current function (main).
	#ifdef DESCRIPTIVE_ERRORS
	if(FreeLocks.size() != (NUM_LOCKS - 1)) {
		print_TrackingDB();
		// One lock is used for the default scope. So, we use (UsedLocks.size() - 1)
		// to see how many locks need to be removed
		if(FreeLocks.size() == NUM_LOCKS) {
			printf("execAtLastAll locks are free. Should have one lock in SLK for default scope\n");
		}
		else {
			if(UsedLocks.size() == 0) {
				printf("execAtLast: UsedLocks = 0, FreeLocks != NUM_LOCKS ??\n");
			}
			else {
				printf("FreeLocks\n");
				printList(FreeLocks);
				printf("UsedLocks\n");
				printList(UsedLocks);
				printf("execAtLast: There are %lu locks that need to be be removed!\n", (UsedLocks.size() - 1));
			}
		}
		#ifdef SILENT_ERRORS
		error_file << "FreeLocks.size(" << FreeLocks.size() << ") != NUM_LOCKS - 1" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(FreeLocks.size() != (NUM_LOCKS - 1)) {
		error_file << "FreeLocks.size(" << FreeLocks.size() << ") != NUM_LOCKS - 1" << std::endl;
	}
	#else
	assert(FreeLocks.size() == (NUM_LOCKS - 1));
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
	if(ScopeLocksAndKeys.size() != 1) {
		if(ScopeLocksAndKeys.size() == 0) {
			printf("execAtLast: SLK is empty. It should contain one element... default scope\n");
		}
		else {
			print_SLK();
			printf("execAtLast: SLK contains %lu elements (should contain just one)\n", 
					ScopeLocksAndKeys.size());
		}
		#ifdef SILENT_ERRORS
		error_file << "execAtLast: SLK.size(" << ScopeLocksAndKeys.size() << ") != 1" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else	
	#ifdef SILENT_ERRORS
	if(ScopeLocksAndKeys.size() != 1) {
		error_file << "execAtLast: SLK.size(" << ScopeLocksAndKeys.size() << ") != 1" << std::endl;
	}
	#else
	assert(ScopeLocksAndKeys.size() == 1);
	#endif
	#endif

	// Remove the init map stuff...
	IntPair lock_key = getTopOfSLK();
	InitInfo* init_map = &InitVec[lock_key.first];
	(*init_map).clear();

	// Remove the remaining lock from SLK.
	removeFromSLK();
	
	#ifdef SILENT_ERRORS
	if(UsedLocks.size() != 0) {
		error_file << "execAtLast: UsedLocks.size(" << UsedLocks.size() << ") != 0" << std::endl;
	}
	#else
	// Now, used locks should be zero.
	assert(UsedLocks.size() == 0);
	#endif


	#ifdef SILENT_ERRORS
	error_file.close();
	#endif
	#endif

	printf("execAtLast: End\n");
}

uint64_t find_lock(unsigned long long base) {
	#ifdef GET_STATS
	FindLock++;
	#endif
	// Some number
	return 0;
}

uint64_t find_key(uint64_t lock) {
	#ifdef GET_STATS
	FindKey++;
	#endif
	// Some number
	return 0;
}

void create_blank_entry(unsigned long long addr) {
	#ifdef GET_STATS
	CreateEntryBlank++;
	#endif

	TrackingDB.create_blank_entry(addr);
}

bool isValidEntry(unsigned long long addr) {
	#ifdef GET_STATS
	ValidEntry++;
	#endif
	TrackingDB.isValidEntry(addr);
}

void v_Ret_create_dummy_entry_UL_Arg(unsigned long long addr) {
	#ifdef GET_STATS
	CreateDummyEntry++;
	#endif
	TrackingDB.create_dummy_entry(addr);
}

void v_Ret_create_entry_UL_Arg_UL_Arg(unsigned long long dest,unsigned long long src) {
	#ifdef GET_STATS
	CreateEntryDest++;
	#endif
	if(src) {
		TrackingDB.isValidEntry(src);

		TrackingDB.copy_entry(dest, src);
	}
	else {
		TrackingDB.create_blank_entry(src);
	}
}

void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long addr, unsigned long long base, unsigned long size, unsigned long long lock) {
	#ifdef GET_STATS
	CreateEntryLong++;
	#endif
	#ifdef DUMMY_LOCK
	if(lock == getDummyLock()) {
		TrackingDB.create_entry(addr, base, base + size, lock, getDummyKey());
	}
	else {
	#endif
		TrackingDB.create_entry(addr, base, base + size, lock, LockDB.getKey(lock));
	#ifdef DUMMY_LOCK
	}
	#endif
}


void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long long base,unsigned long size) {
	#ifdef GET_STATS
	CreateEntry++;
	#endif
	// Pluck the lock and key from the top of scope stack...
	// Or, it would be passed in... this would eventually happen
	// since we would need a lock and key for each scope, or maybe even
	// each variable
	IntPair lock_key = LockDB.getTopOfSLK();
	TrackingDB.create_entry(addr, base, base + size, lock_key.first, lock_key.second);
		
	struct MetaData md;
	md.L = base;
	md.H = base + size;
}

void spatial_check(unsigned long long ptr, unsigned long long lower, unsigned long long upper) {
	#ifdef GET_STATS
	SpatChk++;
	#endif
	#ifdef DUMMY_LOWER_BOUND
	if(lower == DUMMY_LOWER_BOUND && upper == DUMMY_UPPER_BOUND) {
		return;
	}
	#endif

	// More descriptive than just assert
	#ifdef DESCRIPTIVE_ERRORS 
	if(ptr < lower) {
		printf("Out of bounds: ptr: %llu, lower: %llu\n", ptr, lower);
		#ifdef SILENT_ERRORS
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), lower(" << lower << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else
	
	#ifdef SILENT_ERRORS
	if(ptr < lower) {
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), lower(" << lower << ")" << std::endl;
	}
	#else
	assert(ptr >= lower);
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
//	if(ptr > upper) {
	// We should only check until the upper... since the size is added to the
	// base... and comparing with ptr > upper implies that you can still write
	// while the pointer points to the upper limit... which is wrong.
	if(ptr >= upper) {
		printf("Out of bounds: ptr: %llu, upper: %llu\n", ptr, upper);
		#ifdef SILENT_ERRORS
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), upper(" << upper << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else
//	assert(ptr <= upper);

	#ifdef SILENT_ERRORS
	if(ptr >= upper) {
		error_file << "spatial_check: Out of bounds: ptr(" << ptr << "), upper(" << upper << ")" << std::endl;
	}
	#else
	assert(ptr < upper);
	#endif

	#endif
}

void temporal_check(uint64_t lock, uint64_t key) {
	#ifdef GET_STATS
	TempChk++;
	#endif
	if(LockDB.isDummyLock(lock)) {
		return;
	}

	if(key == 0) {
		printf("key = 0; freed this pointer\n");
		assert(0);
	}

	if(LockDB[lock] != key) {
		printf("Temporal Error: Locks[%llu] = %llu, key: %llu\n", lock, LockDB[lock], key);
		assert(0);
	}

	#if 0
	#ifdef DUMMY_LOCK
	if(lock == getDummyLock()) {
		#ifdef SILENT_ERRORS
		error_file << "temporal_check: dummy_lock. No check\n";
		#endif
		return;
	}
	#endif


	#ifdef DESCRIPTIVE_ERRORS
	if(key == 0) {
		printf("key = 0; freed this pointer\n");
		#ifdef SILENT_ERRORS
		error_file << "temporal_check: key at lock(" << lock << ") is zero!" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(key == 0) {
		error_file << "temporal_check: key at lock(" << lock << ") is zero!" << std::endl;
	}
	#else
	assert(key != 0); // To detect double free
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
	if(Locks[lock] != key) {
		printf("Temporal Error: Locks[%llu] = %llu, key: %llu\n", lock, Locks[lock], key);
		#ifdef SILENT_ERRORS
		error_file << "temporal_check: Locks[" << lock << "] = " << Locks[lock] << ", key = " << key << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(Locks[lock] != key) {
		error_file << "temporal_check: Locks[" << lock << "] = " << Locks[lock] << ", key = " << key << std::endl;
	}
	#else
	assert(Locks[lock] == key);
	#endif

	#endif
	#endif
}

#ifdef INT_ARRAY_INDEX
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, int index) {
#else
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, unsigned int index) {
#endif
	#ifdef GET_STATS
	ArrayBnd++;
	#endif
	
	// FIXME: Not checking if casting can still cause the read/write to exceed
	// the bounds.
	#ifdef DESCRIPTIVE_ERRORS
	if(index >= size) {
		printf("Out of bounds. index: %u, size: %u", index, size);

		#ifdef SILENT_ERRORS
		error_file << "array_bound_check: out of bounds: index(" << index << "), size(" << size << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(index >= size) {
		error_file << "array_bound_check: out of bounds: index(" << index << "), size(" << size << std::endl;
	}
	#else
	// index shouldn't be less than zero
	assert(index >= 0);
	assert(index < size);
	#endif

	#endif


	#if 0
	MetaData md = TrackingDB[addr];
	spatial_check(index, md.L, md.H);
	#endif
}

void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long index) {
	#ifdef GET_STATS
	ArrayBndLookup++;
	#endif
	// This is a pointer variable, which is being accessed using a pntr arr ref exp. 
	// So, do a temporal and spatial check
	// If its not a valid entry, then this pointer hasn't been initialized yet.
	
	TrackingDB.isValidEntry(addr);

	if(TrackingDB.isDummyBoundCheck(addr)) {
		return;
	}
	
	struct MetaData md = TrackingDB.get_entry(addr);

	#if 0
	#ifdef DUMMY_LOWER_BOUND
	if(md.L == DUMMY_LOWER_BOUND && md.H == DUMMY_UPPER_BOUND) {
		// Skip the checks
		return;
	}
	#endif
	#endif
	
	// The check here is to see if the index is less than the size... 
	// Not really, a spatial check like we typically do...
	// The index will give ptr + index*sizeof(type). 
	// So, we simply have to check that its below the upper bound.
	assert(index < md.H);
	// It is possible for pointers using pntr arr refs to call this funciton, and use
	// negative array indices. The lower bound check handles that case.
	assert(index >= md.L);
	temporal_check(md.lock, md.key);
}

void v_Ret_check_entry_UL_Arg_UL_Arg(unsigned long long ptr, unsigned long long addr) {
	#ifdef GET_STATS
	CheckEntry++;
	#endif
	struct MetaData md = TrackingDB.get_entry(addr);

	// Check if its a blank entry... that means this is possibly a NULL ptr
	// deref...
	#ifdef DESCRIPTIVE_ERRORS
	if(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0) {
		printf("check_entry: blank_entry derefed. NULL pointer deref\n");

		#ifdef SILENT_ERRORS
		error_file << "check_entry: blank_entry derefed at addr(" << addr << ")" << std::endl;
		#else
		assert(0);
		#endif
	}
	#else

	#ifdef SILENT_ERRORS
	if(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0) {
		error_file << "check_entry: blank_entry derefed at addr(" << addr << ")" << std::endl;
	}
	#else
	assert(!(md.L == 0 && md.H == 0 && md.lock == 0 && md.key == 0));
	#endif

	#endif

	#ifdef DESCRIPTIVE_ERRORS
	//printf("Temporal check\n");
	#endif
	temporal_check(md.lock, md.key);
	#ifdef DESCRIPTIVE_ERRORS
	//printf("Spatial check\n");
	#endif
	spatial_check(ptr, md.L, md.H);

}

void remove_entry(struct __Pb__v__Pe___Type input) {
	#ifdef GET_STATS
	RemoveEntry++;
	#endif
	struct MetaData md = TrackingDB.get_entry(input.addr);

	LockDB.remove_lock(md.lock);

	#if 0
	#ifdef DUMMY_LOCK
	if(md.lock != DUMMY_LOCK) {
		// clear the lock
		LockDB.remove_lock(md.lock);
	}
	#else
	LockDB.remove_lock(md.lock);
	#endif
	#endif
	
	TrackingDB.remove_entry(input.addr);
}

void create_entry_with_new_lock(unsigned long long addr, unsigned long long base, unsigned long size) {
	#ifdef GET_STATS
	CreateEntryLock++;
	#endif
	IntPair lock_key = LockDB.insert_lock();
	TrackingDB.create_entry(addr, base, base + size, lock_key.first, lock_key.second);
}


struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(unsigned long size) {
	#ifdef GET_STATS
	MallocOvl++;
	#endif
	struct __Pb__v__Pe___Type output;
	output.ptr = malloc(size);
	output.addr = reinterpret_cast<unsigned long long>(&output.ptr);
	create_entry_with_new_lock(output.addr, reinterpret_cast<unsigned long long>(output.ptr), size);
	#ifdef DEBUG	
	printf("malloc overload\n");
	printf("output.ptr: %llu, output.addr: %llu\n", reinterpret_cast<unsigned long long>(output.ptr),
													output.addr);
	#endif
	return output;
}

void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long dest, unsigned long long src) {
	#ifdef GET_STATS
	CreateEntrySrc++;
	#endif
	if(!TrackingDB.entryExists(src)) {
		return;
	}
	TrackingDB.copy_entry(dest, src);
}

void lower_bound_check(unsigned long long ptr, unsigned long long addr) {
	#ifdef GET_STATS
	LowerBnd++;
	#endif
	IntPair lower_upper = TrackingDB.get_bounds(addr);
	assert(lower_upper.first == ptr);	
}

struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_realloc_overload___Pb__v__Pe___Type_Arg_Ul_Arg(struct __Pb__v__Pe___Type str, unsigned long size) {

	#ifdef GET_STATS
	ReallocOvl++;
	#endif

	// Since we are "reallocating", the str.ptr should either be NULL or 
	// a valid entry.
	if(str.ptr != NULL) {
		TrackingDB.isValidEntry(str.addr);
	}

	// Also check if str.ptr actually points to the lower bound of the allocation
	lower_bound_check((unsigned long long)str.ptr, str.addr);

	// Do a realloc and update the bounds.
	str.ptr = realloc(str.ptr, size);
	// Update the bounds
	//v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(str.addr, reinterpret_cast<unsigned long long>(str.ptr), size);
	// FIXME: Might need to zero out the previous lock, since we are taking a new lock here...
	create_entry_with_new_lock(str.addr, reinterpret_cast<unsigned long long>(str.ptr), size);

	return str;
}


void v_Ret_null_check_UL_Arg(unsigned long long ptr) {

	#ifdef GET_STATS
	NullChk++;
	#endif
	assert((void*)ptr != NULL);
}


void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input) {

	#ifdef GET_STATS
	FreeOvl++;
	#endif
	v_Ret_check_entry_UL_Arg_UL_Arg((unsigned long long)input.ptr, input.addr);
	// Also check if input.ptr is greater than base... in which case, free
	// won't be able to get the correct metadata... free can only be called
	// when the pointer points to the base of the allocation.
	lower_bound_check((unsigned long long)input.ptr, input.addr);
	remove_entry(input);
	free(input.ptr);

}

#if 1
#ifdef __cplusplus
}
#endif
#endif

//------------------------------

