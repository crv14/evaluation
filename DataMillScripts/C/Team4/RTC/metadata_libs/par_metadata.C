#include "par_metadata.h"

// Making this 1 ensures that the 
// function names are not mangled
#if 0
#ifdef __cplusplus
extern "C" {
#endif
#endif

#define PARALLEL_MD


const char* getStringForTy(REQ_TYPE ty) {
	
	std::string m;

	switch(ty) {
	case ENTER_SCOPE: m = "ENTER_SCOPE"; break; 
	case EXIT_SCOPE : m = "EXIT_SCOPE"; break;
	case UPDATE_INITINFO: m = "UPDATE_INITINFO"; break;
	case CHECK_INITINFO: m = "CHECK_INITINFO"; break;
	case CREATE_DUMMY_ENTRY: m = "CREATE_DUMMY_ENTRY"; break;
	case CREATE_ENTRY_DEST_SRC: m = "CREATE_ENTRY_DEST_SRC"; break;
	case CREATE_ENTRY_4: m = "CREATE_ENTRY_4"; break;
	case CREATE_ENTRY_3: m = "CREATE_ENTRY_3"; break;
	case ARRAY_BOUND_CHECK: m = "ARRAY_BOUND_CHECK"; break;
	case ARRAY_BOUND_CHECK_USING_LOOKUP: m = "ARRAY_BOUND_CHECK_USING_LOOKUP"; break;
	case CHECK_ENTRY: m = "CHECK_ENTRY"; break;
	case MALLOC_OVERLOAD: m = "MALLOC_OVERLOAD"; break;
	case CREATE_ENTRY_IF_SRC_EXISTS: m = "CREATE_ENTRY_IF_SRC_EXISTS"; break;
	case REALLOC_OVERLOAD_1: m = "REALLOC_OVERLOAD_1"; break;
	case REALLOC_OVERLOAD_2: m = "REALLOC_OVERLOAD_2"; break;
	case NULL_CHECK: m = "NULL_CHECK"; break;
	case FREE_OVERLOAD: m = "FREE_OVERLOAD"; break;
	case EXEC_AT_FIRST: m = "EXEC_AT_FIRST"; break;
	case EXEC_AT_LAST: m = "EXEC_AT_LAST"; break;
	default: m = "NOT YET HANDLED";
	}

	return m.c_str();
}

void setupReadFifo() {
	// fifo should already be available. 
	// just open it for reading
	char buf[128];
	read_fd = open(fifopath, O_RDONLY);
	if(read_fd == 0) {
		// something is wrong
		printf("can't open fifo for reading\n");
		return;
	}
}

void setupWriteFifo() {
	// fifo should already be available
	// just open it for writing
	char buf[128];
	write_fd = open(fifopath, O_WRONLY);
	if(write_fd == 0) {
		printf("can't open fifo for writing\n");
	}
}

void* metadata_checker(void* m) {
	#ifdef THREADX_DEBUG
	printf("\t\t\t\t%s: metadata_checker\n", (char*)m);	
	#endif
	
	while(1) {
		QUEUE_ARG_TYPE arg;
		// Check for the finish packet
		int err = read(read_fd, &arg, sizeof(QUEUE_ARG_TYPE));
		if(arg.ty == EXEC_AT_LAST) {
			metadata_execAtLast();
			break;
		}
		callApprInstrFn(arg);
	}

	return NULL;
}




void addToSendQ(QUEUE_ARG_TYPE args) {
	// Guarding the push with a lock to disallow multiple threads from pushing
	// to the same thread.
	write(write_fd, &args, sizeof(QUEUE_ARG_TYPE));	
}

void enqueueIntoSendQWithArgs(REQ_TYPE ty, ARG_TYPE args) {
	#ifdef DEBUG
	printf("enqueueIntoSendWithArgs: %s\n", getStringForTy(ty));
	#endif
	QUEUE_ARG_TYPE qargs;
	qargs.ty = ty;
	qargs.ar = args;
	addToSendQ(qargs);
}

void enqueueIntoSendQ(REQ_TYPE ty) {
	#ifdef DEBUG
	printf("enqueueIntoSend: %s\n", getStringForTy(ty));
	#endif
	QUEUE_ARG_TYPE qargs;
	qargs.ty = ty;
	addToSendQ(qargs);
}

void callApprInstrFn(QUEUE_ARG_TYPE arg) {

	REQ_TYPE ty = arg.ty;
	ARG_TYPE ar = arg.ar;

	switch(ty) {
	case ENTER_SCOPE: metadata_EnterScope(); break; 
	case EXIT_SCOPE : metadata_ExitScope(); break;
	case UPDATE_INITINFO: metadata_updateInitInfo(ar.arg1, ar.arg2); break;
	case CHECK_INITINFO:  metadata_checkInitInfo(ar.arg1, ar.arg2); break;
	case CREATE_DUMMY_ENTRY: metadata_create_dummy_entry(ar.arg1); break;
	case CREATE_ENTRY_DEST_SRC: metadata_create_entry_dest_src(ar.arg1, ar.arg2); break;
	case CREATE_ENTRY_4: metadata_create_entry_4(ar.arg1, ar.arg2, ar.arg3, ar.arg4); break;
	case CREATE_ENTRY_3: metadata_create_entry_3(ar.arg1, ar.arg2, ar.arg3); break;
	case ARRAY_BOUND_CHECK: metadata_array_bound_check(ar.arg1, ar.arg2); break;
	case ARRAY_BOUND_CHECK_USING_LOOKUP: metadata_array_bound_check_using_lookup(ar.arg1, ar.arg2); break;
	case CHECK_ENTRY: metadata_check_entry(ar.arg1, ar.arg2); break;
	case MALLOC_OVERLOAD: metadata_malloc_overload(ar.arg1, ar.arg2, ar.arg3); break;
	case CREATE_ENTRY_IF_SRC_EXISTS: metadata_create_entry_if_src_exists(ar.arg1, ar.arg2); break;
	case REALLOC_OVERLOAD_1: metadata_realloc_overload_1(ar.arg1, ar.arg2); break;
	case REALLOC_OVERLOAD_2: metadata_realloc_overload_2(ar.arg1, ar.arg2, ar.arg3); break;
	case NULL_CHECK: break; // FIXME: Null check is not offloaded.
	case FREE_OVERLOAD: metadata_free_overload(ar.arg1, ar.arg2); break;
	case EXEC_AT_FIRST: metadata_execAtFirst(); break;
//	case EXEC_AT_LAST: metadata_execAtLast(); break;
	case EXEC_AT_LAST: printf("execAtLast shouldn't be called from callApprInstrFn\n"); break;
	default: printf("NOT YET HANDLED. Exiting\n"); exit(1);
	}
}


void v_Ret_execAtFirst() {
	
	setupWriteFifo();
	enqueueIntoSendQ(EXEC_AT_FIRST);

}

void v_Ret_EnterScope() {
	#ifdef PARALLEL_MD
	enqueueIntoSendQ(ENTER_SCOPE);
	return;
	#else
	metadata_EnterScope();
	#endif
}

void v_Ret_ExitScope() {
	#ifdef PARALLEL_MD
	enqueueIntoSendQ(EXIT_SCOPE);
	return;
	#else
	metadata_ExitScope();
	#endif	
}

void v_Ret_update_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr) {
	#ifdef PARALLEL_MD
	ARG_TYPE args = {lock, addr, 0, 0, 0};
	enqueueIntoSendQWithArgs(UPDATE_INITINFO, args);
	return;
	#else
	metadata_updateInitInfo(lock, addr);
	#endif
}

void v_Ret_check_initinfo_L_Arg_UL_Arg(long long lock, unsigned long long addr) {
	#ifdef PARALLEL_MD
	ARG_TYPE args = {lock, addr, 0, 0, 0};
	enqueueIntoSendQWithArgs(CHECK_INITINFO, args);
	return;
	#else
	metadata_checkInitInfo(lock, addr);
	#endif

}

void v_Ret_execAtLast() {
	enqueueIntoSendQ(EXEC_AT_LAST);
	return;
}

void v_Ret_create_dummy_entry_UL_Arg(unsigned long long addr) {
	#ifdef PARALLEL_MD
	ARG_TYPE args = {addr, 0, 0, 0, 0};
	enqueueIntoSendQWithArgs(CREATE_DUMMY_ENTRY, args);
	return;
	#else
	metadata_create_dummy_entry(addr);
	#endif 
}

void v_Ret_create_entry_UL_Arg_UL_Arg(unsigned long long dest,unsigned long long src) {
	#ifdef PARALLEL_MD
	ARG_TYPE args = {dest, src, 0, 0, 0};
	enqueueIntoSendQWithArgs(CREATE_ENTRY_DEST_SRC, args);
	return;
	#else
	metadata_create_entry_dest_src(dest, src);
	#endif
}

void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long addr, unsigned long long base, unsigned long size, unsigned long long lock) {
	#ifdef PARALLEL_MD
	ARG_TYPE args = {addr, base, size, lock, 0};
	enqueueIntoSendQWithArgs(CREATE_ENTRY_4, args);
	return;
	#else
	metadata_create_entry_4(addr, base, size, lock);
	#endif
}

void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long long base,unsigned long size) {
	
	#ifdef PARALLEL_MD
	ARG_TYPE args = {addr, base, size, 0, 0};
	enqueueIntoSendQWithArgs(CREATE_ENTRY_3, args);
	return;
	#else
	metadata_create_entry_3(addr, base, size);
	#endif

}
#ifdef INT_ARRAY_INDEX
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, int index) {
#else
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int size, unsigned int index) {
#endif

	// FIXME: Might need the sign of index to be maintained. We are converting
	// it to an unsigned long long here in the args. That might create problems
	#ifdef PARALLEL_MD
	ARG_TYPE args = {size, index, 0, 0, 0};
	enqueueIntoSendQWithArgs(ARRAY_BOUND_CHECK, args);
	return;
	#else
	metadata_array_bound_check(size, index);
	#endif	
}


void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long addr,unsigned long index) {

	#ifdef PARALLEL_MD
	ARG_TYPE args = {addr, index, 0, 0, 0};
	enqueueIntoSendQWithArgs(ARRAY_BOUND_CHECK_USING_LOOKUP, args);
	return;
	#else
	metadata_array_bound_check_using_lookup(addr, index);
	#endif
}

void v_Ret_check_entry_UL_Arg_UL_Arg(unsigned long long ptr, unsigned long long addr) {
	
	#ifdef PARALLEL_MD
	ARG_TYPE args = {ptr, addr, 0, 0, 0};
	enqueueIntoSendQWithArgs(CHECK_ENTRY, args);
	return;
	#else
	metadata_check_entry(ptr, addr);
	#endif
}

struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(unsigned long size) {


	struct __Pb__v__Pe___Type output;
	output.ptr = malloc(size);
	output.addr = reinterpret_cast<unsigned long long>(&output.ptr);

	#ifdef PARALLEL_MD
	ARG_TYPE args = {output.addr, (unsigned long long)output.ptr, size, 0, 0};
	enqueueIntoSendQWithArgs(MALLOC_OVERLOAD, args);
	return output;
	#else
	metadata_malloc_overload(output.addr, (unsigned long long)output.ptr, size);
	return output;
	#endif

}

void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long dest, unsigned long long src) {
	
	#ifdef PARALLEL_MD
	ARG_TYPE args = {dest, src, 0, 0, 0};
	enqueueIntoSendQWithArgs(CREATE_ENTRY_IF_SRC_EXISTS, args);
	return;
	#else
	metadata_create_entry_if_src_exists(dest, src);
	#endif

}

struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_realloc_overload___Pb__v__Pe___Type_Arg_Ul_Arg(struct __Pb__v__Pe___Type str, unsigned long size) {
	
	// FIXME: Tricky situation... metadata checks before we can reallocate. Maybe we 
	// just push the checks into the checker thread, and reallocate before checking
	#ifdef PARALLEL_MD
	// First part ... check before the realloc
	ARG_TYPE args = {(unsigned long long)str.ptr, str.addr, 0, 0, 0};
	enqueueIntoSendQWithArgs(REALLOC_OVERLOAD_1, args);

	str.ptr = realloc(str.ptr, size);
	// Second part ... metadata insert after realloc
	ARG_TYPE args2 = {(unsigned long long)str.ptr, str.addr, size, 0, 0};
	enqueueIntoSendQWithArgs(REALLOC_OVERLOAD_2, args2);
	return str;
	#else
	metadata_realloc_overload_1(ptr, addr);
	// Do a realloc and update the bounds.
	str.ptr = realloc(str.ptr, size);
	metadata_realloc_overload_2(ptr, addr, size);
	return str;
	#endif
}

void v_Ret_null_check_UL_Arg(unsigned long long ptr) {
	#ifdef DEBUG
	printf("null_check\n");
	#endif
	// FIXME: This check can be done here itself... not changing it.
	assert((void*)ptr != NULL);
}

void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input) {

	#ifdef PARALLEL_MD
	ARG_TYPE args = {(unsigned long long)input.ptr, input.addr, 0, 0, 0};
	enqueueIntoSendQWithArgs(FREE_OVERLOAD, args);
	free(input.ptr);
	return;
	#else
	metadata_free_overload((unsigned long long)input.ptr, input.addr);
	free(input.ptr);
	#endif

}


#if 0
#ifdef __cplusplus
}
#endif
#endif
