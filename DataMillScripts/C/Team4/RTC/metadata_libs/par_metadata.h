#include "metadata.h"

// For the parallel metadata checker part
typedef enum request_type {
	ENTER_SCOPE,
	EXIT_SCOPE,
	UPDATE_INITINFO,
	CHECK_INITINFO,
	CREATE_DUMMY_ENTRY,
	CREATE_ENTRY_DEST_SRC,
	CREATE_ENTRY_4,
	CREATE_ENTRY_3,
	ARRAY_BOUND_CHECK,
	ARRAY_BOUND_CHECK_USING_LOOKUP,
	CHECK_ENTRY,
	MALLOC_OVERLOAD,
	CREATE_ENTRY_IF_SRC_EXISTS,
	REALLOC_OVERLOAD_1,
	REALLOC_OVERLOAD_2,
	NULL_CHECK,
	FREE_OVERLOAD,
	EXEC_AT_FIRST,
	EXEC_AT_LAST
} REQ_TYPE;

typedef struct argument_type {
	unsigned long long arg1;
	unsigned long long arg2;
	unsigned long long arg3;
	unsigned long long arg4;
	unsigned long long arg5;
} ARG_TYPE;

typedef struct queue_argument_type {
	REQ_TYPE ty;
	ARG_TYPE ar;
} QUEUE_ARG_TYPE;



// Create the queue that will hold the set of check requests from the main
// thread
typedef std::queue<QUEUE_ARG_TYPE> ArgQueue;
// Remember: elements are "push"ed into the back,
// and "pop"ped from the front

ArgQueue SendQ;
// we set finish to false at the beginning (could also do it in
// execAtFirst), and set it to true at the end of execAtLast
static bool finish = false;

// For parallel metadata checking
void enqueueIntoSendQ(REQ_TYPE ty);
void enqueueIntoSendQWithArgs(REQ_TYPE ty, ARG_TYPE args);
void addToSendQ(QUEUE_ARG_TYPE args);
void* metadata_checker(void* m);
void setupWriteFifo();
void setupReadFifo();

const char* fifopath = "/Users/vanka1/research/compilers/rose/rose_build/projects/RTC/metadata_fifo";
int read_fd;
int write_fd;

const char* getStringForTy(REQ_TYPE ty);
void callApprInstrFn(QUEUE_ARG_TYPE arg);
