#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define PTR_SIZE 100
#define PTR2_SIZE 10
#define PTR3_SIZE 10
#define OUT_OF_BOUNDS_ACCESS 1

struct __Pb__v__Pe___GenType 
{
  void *ptr;
  unsigned long long addr;
}
;
struct __Pb__v__Pe___GenType __Pb__v__Pe___GenType_Ret_malloc_overload_Ul_Arg(unsigned long input1);

struct __Pb__i__Pe___GenType 
{
  int *ptr;
  unsigned long long addr;
}
;

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_Cast___Pb__v__Pe___GenType_Arg(struct __Pb__v__Pe___GenType input1)
{
  struct __Pb__i__Pe___GenType output;
  output . __Pb__i__Pe___GenType::ptr = ((int *)input1 . __Pb__v__Pe___GenType::ptr);
  output . __Pb__i__Pe___GenType::addr = input1 . __Pb__v__Pe___GenType::addr;
  return output;
}
void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_create_struct___Pb__i__Pe___Arg_UL_Arg(int *input1,unsigned long long input2)
{
  struct __Pb__i__Pe___GenType output;
  output . __Pb__i__Pe___GenType::ptr = input1;
  output . __Pb__i__Pe___GenType::addr = input2;
  return output;
}

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___GenType_Arg(int **input1,struct __Pb__i__Pe___GenType input2)
{
   *input1 = input2 . __Pb__i__Pe___GenType::ptr;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),((unsigned long long )input2 . __Pb__i__Pe___GenType::addr));
  return __Pb__i__Pe___GenType_Ret_create_struct___Pb__i__Pe___Arg_UL_Arg(input2 . __Pb__i__Pe___GenType::ptr,((unsigned long long )input1));
}
static long global_var0_17_2 = 0L;
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3,unsigned long long input4);

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(int *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__i__Pe___GenType_Ret_create_struct___Pb__i__Pe___Arg_UL_Arg(input1,input2);
}

static int i_Ret_NotEqual___Pb__i__Pe___Arg___Pb__i__Pe___GenType_Arg(int *input1,struct __Pb__i__Pe___GenType input2)
{
  return input1 != input2 . __Pb__i__Pe___GenType::ptr;
}

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___Arg_UL_Arg(int **input1,int *input2,unsigned long long input3)
{
   *input1 = input2;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),input3);
  return __Pb__i__Pe___GenType_Ret_create_struct___Pb__i__Pe___Arg_UL_Arg(input2,((unsigned long long )input1));
}
void v_Ret_check_entry_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);
void v_Ret_null_check_UL_Arg(unsigned long long input1);

static int *__Pb__i__Pe___Ret_deref_check___Pb__i__Pe___Arg_UL_Arg(int *input1,unsigned long long input2)
{
  v_Ret_check_entry_UL_Arg_UL_Arg(((unsigned long long )input1),input2);
  v_Ret_null_check_UL_Arg(((unsigned long long )input1));
  return input1;
}

static struct __Pb__i__Pe___GenType __Pb__i__Pe___GenType_Ret_Increment___Pb____Pb__i__Pe____Pe___Arg(int **input1)
{
  ++ *input1;
  struct __Pb__i__Pe___GenType output;
  output . __Pb__i__Pe___GenType::ptr =  *input1;
  output . __Pb__i__Pe___GenType::addr = ((unsigned long long )input1);
  return output;
}
static long global_var2_70_2 = 0L;
static long global_var4_72_9 = 0L;

struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType 
{
  FILE *ptr;
  unsigned long long addr;
}
;

static struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType L1384R_Ret_create_struct___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg(FILE *input1,unsigned long long input2)
{
  struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType output;
  output . __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType::ptr = input1;
  output . __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType::addr = input2;
  return output;
}

static struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType L1384R_Ret_Cast___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(FILE *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return L1384R_Ret_create_struct___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg(input1,input2);
}

static FILE *__Pb__FILE_IO_FILE__typedef_declaration__Pe___Ret_return_pointer_L1384R_Arg(struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType input1)
{
  return input1 . __Pb__FILE_IO_FILE__typedef_declaration__Pe___GenType::ptr;
}

static struct __Pb__v__Pe___GenType __Pb__v__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg(int *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___GenType output;
  output . __Pb__v__Pe___GenType::ptr = ((void *)input1);
  output . __Pb__v__Pe___GenType::addr = input2;
  return output;
}
void v_Ret_free_overload___Pb__v__Pe___GenType_Arg(struct __Pb__v__Pe___GenType input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();

int main()
{
  v_Ret_execAtFirst();
  int *ptr;
  __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___GenType_Arg(&ptr,__Pb__i__Pe___GenType_Ret_Cast___Pb__v__Pe___GenType_Arg(__Pb__v__Pe___GenType_Ret_malloc_overload_Ul_Arg(((unsigned long )(100 * sizeof(int ))))));
  int *ptr2;
  __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___GenType_Arg(&ptr2,__Pb__i__Pe___GenType_Ret_Cast___Pb__v__Pe___GenType_Arg(__Pb__v__Pe___GenType_Ret_malloc_overload_Ul_Arg(((unsigned long )(10 * sizeof(int ))))));
  i_Ret_NotEqual___Pb__i__Pe___Arg___Pb__i__Pe___GenType_Arg(ptr2,__Pb__i__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((int *)global_var0_17_2),((unsigned long long )(&global_var0_17_2)),((unsigned long )(sizeof(global_var0_17_2))),((unsigned long long )50)))?((void )0) : __assert_fail("ptr2 != __null","pointer_example.cpp",17,__PRETTY_FUNCTION__);
  int *start_ptr;
  __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___Arg_UL_Arg(&start_ptr,((int *)ptr),((unsigned long long )(&ptr)));
  int *start_ptr2;
  __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___Arg_UL_Arg(&start_ptr2,((int *)ptr2),((unsigned long long )(&ptr2)));
// Crossing the boundary of ptr. The condition should
// be less than, not less than or equal to
// ptr[PTR_SIZE] is an out-of-bounds access
//	for(int index = 0; index <= (PTR_SIZE + OUT_OF_BOUNDS_ACCESS); index++) {
  for (int index = 0; index < 100; index++) {
     *__Pb__i__Pe___Ret_deref_check___Pb__i__Pe___Arg_UL_Arg(ptr,((unsigned long long )(&ptr))) = index;
    __Pb__i__Pe___GenType_Ret_Increment___Pb____Pb__i__Pe____Pe___Arg(&ptr);
  }
// Resetting ptr to start_ptr, so that it points to the beginning
// of the allocation
  __Pb__i__Pe___GenType_Ret_assign_and_copy___Pb____Pb__i__Pe____Pe___Arg___Pb__i__Pe___Arg_UL_Arg(&ptr,((int *)start_ptr),((unsigned long long )(&start_ptr)));
// Printing what we wrote above
//	for(int index = 0; index <= (PTR_SIZE + OUT_OF_BOUNDS_ACCESS); index++) {
  for (int index = 0; index < 100; index++) {
    printf("ptr[%d]=%d\n",index, *__Pb__i__Pe___Ret_deref_check___Pb__i__Pe___Arg_UL_Arg(ptr,((unsigned long long )(&ptr))));
    __Pb__i__Pe___GenType_Ret_Increment___Pb____Pb__i__Pe____Pe___Arg(&ptr);
  }
#if 0
// Resetting ptr to start_ptr, so that it points to the beginning
// of the allocation
// Memsetting ptr and ptr2 allocations, in one go.
// This is also crossing the boundaries of ptr. It assumes that
// ptr and ptr2 are in contiguous locations
// Resetting ptr to start_ptr, so that it points to the beginning
// of the allocation
// Printing ptr and ptr2 *and* one more beyond ptr2, all using 
// ptr! This still works since malloc asks for more than it needs
// always.
#endif
  i_Ret_NotEqual___Pb__i__Pe___Arg___Pb__i__Pe___GenType_Arg(ptr2,__Pb__i__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((int *)global_var2_70_2),((unsigned long long )(&global_var2_70_2)),((unsigned long )(sizeof(global_var2_70_2))),((unsigned long long )50)))?((void )0) : __assert_fail("ptr2 != __null","pointer_example.cpp",70,__PRETTY_FUNCTION__);
  printf("Before free ptr2\n");
  fflush(__Pb__FILE_IO_FILE__typedef_declaration__Pe___Ret_return_pointer_L1384R_Arg(L1384R_Ret_Cast___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((FILE *)global_var4_72_9),((unsigned long long )(&global_var4_72_9)),((unsigned long )(sizeof(global_var4_72_9))),((unsigned long long )50))));
  v_Ret_free_overload___Pb__v__Pe___GenType_Arg(__Pb__v__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg(ptr2,((unsigned long long )(&ptr2))));
  v_Ret_free_overload___Pb__v__Pe___GenType_Arg(__Pb__v__Pe___GenType_Ret_Cast___Pb__i__Pe___Arg_UL_Arg(start_ptr,((unsigned long long )(&start_ptr))));
#if 0
	#if 0
// Retrying the print above, after freeing ptr2. This should
// crash--- and it does!
	#endif
// Allocating another pointer
// This allocation might take the place of ptr2. In this case,
// printing ptr beyond its boundaries should be okay
// Nope this also crashes!
#endif	
  v_Ret_execAtLast();
  return 1;
}
