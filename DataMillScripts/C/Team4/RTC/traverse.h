#include "util.h"

//#define JUST_TRAVERSE
//#define TRAVERSE_INPUT_FILE_TOP
//#define SYNTH_DEBUG

namespace Trav {

	extern NodeContainer NodesToInstrument;	
	extern NodeContainer DeclsToClone;
	extern SgProject* project;

	bool candidateFnCall(SgNode* node);
	bool candidateFnDecl(SgFunctionDeclaration* fn_decl);
	void CheckExprSanity(SgExpression* expr);
	bool NeedsToBeOverloaded(SgNode* node);
	bool isQualifyingLibCall(SgFunctionCallExp* fncall);


	class TopBotTrack2 : public AstTopDownBottomUpProcessing<Util::nodeType, Util::nodeType> {
	protected:
	Util::nodeType virtual evaluateSynthesizedAttribute(SgNode* node, Util::nodeType inh, 
	SynthesizedAttributesList synList);

	Util::nodeType virtual evaluateInheritedAttribute(SgNode* node, Util::nodeType inh);
};	

	void traverse(SgProject* proj);

}
