// RUN: test.sh -e -t %t %s
//
// TEST: free-001
// XFAIL: darwin,linux
//
// Description:
//  Test invalid memory deallocations
//
#include <stdio.h>
#include <stdlib.h>

struct __Pb__v__Pe___Type 
{
  void *ptr;
  unsigned long long addr;
}
;

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output.ptr = ((void *)input1);
  output.addr = input2;
  return output;
}
void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1);
void v_Ret_create_dummy_entry_UL_Arg(unsigned long long input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();
char *array;

int main(int argc,char **argv)
{
  v_Ret_execAtFirst();
  v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv)));
  int index_argc;
  for (index_argc = 0; index_argc < argc; ++index_argc) 
    v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv[index_argc])));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(array,((unsigned long long )(&array))));
  v_Ret_execAtLast();
  return 0;
}
