#!/bin/sh

#Team 2 Benchmarks - violation checks
./output/Team2/Bench1/benchmark1.o > output/Team2/Bench1/output.txt
./output/Team2/Bench2/benchmark2.o > output/Team2/Bench2/output.txt
./output/Team2/Bench3/benchmark3.o > output/Team2/Bench3/output.txt

#Team 4 Benchmarks - violation checks
./output/Team4/Bench1/benchmark1.o > output/Team4/Bench1/output.txt
./output/Team4/Bench2/benchmark2.o > output/Team4/Bench2/output.txt
./output/Team4/Bench3/benchmark3.o 12221 > output/Team4/Bench3/output.txt
#./output/Team4/Bench4/benchmark4.o < benchmarks/Team4/Bench4/src/key "helloworld" > output/Team4/Bench4/output.txt
#./output/Team4/Bench5/benchmark5.o > output/Team4/Bench5/output.txt


#Team 2 Benchmarks - execution time checks
(time ./output/Team2/Bench1/benchmark1.o) &> output/Team2/Bench1/RTC_exec_time.txt
(time ./output/Team2/Bench2/benchmark2.o) &> output/Team2/Bench2/RTC_exec_time.txt
(time ./output/Team2/Bench3/benchmark3.o) &> output/Team2/Bench3/RTC_exec_time.txt

(time ./output/Team2/Bench1/benchmark1_original.o) &> output/Team2/Bench1/original_exec_time.txt
(time ./output/Team2/Bench2/benchmark2_original.o) &> output/Team2/Bench2/original_exec_time.txt
(time ./output/Team2/Bench3/benchmark3_original.o) &> output/Team2/Bench3/original_exec_time.txt

#Team 4 Benchmarks - execution time checks
(time ./output/Team4/Bench1/benchmark1.o) &> output/Team4/Bench1/RTC_exec_time.txt
(time ./output/Team4/Bench2/benchmark2.o) &> output/Team4/Bench2/RTC_exec_time.txt
(time ./output/Team4/Bench3/benchmark3.o 12221) &> output/Team4/Bench3/RTC_exec_time.txt
(time ./output/Team4/Bench4/benchmark4.o < benchmarks/Team4/Bench4/src/key "helloworld") &> output/Team4/Bench4/RTC_exec_time.txt
(time ./output/Team4/Bench5/benchmark5.o) &> output/Team4/Bench5/RTC_exec_time.txt

(time ./output/Team4/Bench1/benchmark1_original.o) &> output/Team4/Bench1/original_exec_time.txt
(time ./output/Team4/Bench2/benchmark2_original.o) &> output/Team4/Bench2/original_exec_time.txt
(time ./output/Team4/Bench3/benchmark3_original.o 12221) &> output/Team4/Bench3/original_exec_time.txt
#(time ./output/Team4/Bench4/benchmark4_original.o < benchmarks/Team4/Bench4/src/key "helloworld") &> output/Team4/Bench4/original_exec_time.txt
#(time ./output/Team4/Bench5/benchmark5_original.o) &> output/Team4/Bench5/original_exec_time.txt

#Team 2 Benchmarks - memory checks
(/usr/bin/time ./output/Team2/Bench1/benchmark1.o) &> output/Team2/Bench1/RTC_memory_usage.txt
( /usr/bin/time ./output/Team2/Bench2/benchmark2.o) &> output/Team2/Bench2/RTC_memory_usage.txt
( /usr/bin/time ./output/Team2/Bench3/benchmark3.o) &> output/Team2/Bench3/RTC_memory_usage.txt

( /usr/bin/time ./output/Team2/Bench1/benchmark1_original.o) &> output/Team2/Bench1/original_memory_usage.txt
( /usr/bin/time ./output/Team2/Bench2/benchmark2_original.o) &> output/Team2/Bench2/original_memory_usage.txt
( /usr/bin/time ./output/Team2/Bench3/benchmark3_original.o) &> output/Team2/Bench3/original_memory_usage.txt

#Team 4 Benchmarks - memory checks
( /usr/bin/time ./output/Team4/Bench1/benchmark1.o) &> output/Team4/Bench1/RTC_memory_usage.txt
( /usr/bin/time ./output/Team4/Bench2/benchmark2.o) &> output/Team4/Bench2/RTC_memory_usage.txt
( /usr/bin/time ./output/Team4/Bench3/benchmark3.o 12221) &> output/Team4/Bench3/RTC_memory_usage.txt
#( /usr/bin/time ./output/Team4/Bench4/benchmark4.o < benchmarks/Team4/Bench4/src/key "helloworld") &> output/Team4/Bench4/RTC_memory_usage.txt
#( /usr/bin/time ./output/Team4/Bench5/benchmark5.o) &> output/Team4/Bench5/RTC_memory_usage.txt

( /usr/bin/time ./output/Team4/Bench1/benchmark1_original.o) &> output/Team4/Bench1/original_memory_usage.txt
( /usr/bin/time ./output/Team4/Bench2/benchmark2_original.o) &> output/Team4/Bench2/original_memory_usage.txt
( /usr/bin/time ./output/Team4/Bench3/benchmark3_original.o 12221) &> output/Team4/Bench3/original_memory_usage.txt
#( /usr/bin/time ./output/Team4/Bench4/benchmark4_original.o < benchmarks/Team4/Bench4/src/key "helloworld") &> output/Team4/Bench4/original_memory_usage.txt
#( /usr/bin/time ./output/Team4/Bench5/benchmark5_original.o) &> output/Team4/Bench5/original_memory_usage.txt


