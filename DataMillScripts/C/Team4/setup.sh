#!/bin/sh

emerge sudo
emerge =sys-devel/gcc-4.4.7 #Need to have a slightly older version of gcc or ROSE will complain.
emerge sys-process/time
gcc-config i686-pc-linux-gnu-4.4.7
wget http://sourceforge.net/projects/boost/files/boost/1.47.0/boost_1_47_0.tar.gz
tar xvzf boost_1_47_0.tar.gz
cd boost_1_47_0
sudo ./bootstrap.sh
sed -i 's/TIME_UTC/TIME_UTC_/g' boost/thread/xtime.hpp
sed -i 's/TIME_UTC/TIME_UTC_/g' libs/thread/src/pthread/timeconv.inl
sudo ./bjam install
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/boost_1_47_0/stage/lib/
cd ..
git clone https://github.com/rose-compiler/rose.git
cd rose
./build
cd ..
mkdir rose_build
cd rose_build
./../rose/configure --with-boost=/usr/local/ --with-haskell=no --with-java=no --with-fortran=no CXX=g++-4.4.7 CC=gcc-4.4.7
sed -i 's/TIME_UTC/TIME_UTC_/g' ../rose/tests/parallelASTMerge.C
cp ../rose/src/frontend/SageIII/sage_support/sage_support.cpp src/frontend/SageIII/sage_support/sage_support.cpp
cp ../rose/src/frontend/SageIII/sage_support/sage_support.h src/frontend/SageIII/sage_support/sage_support.h
make -j 10
make install -j 10
cd ..
cd RTC
make
cd metadata_libs
g++ metadata_alt.C -c -fpic -m32
g++ -shared -o metadata_alt.so metadata_alt.o -m32
g++ metalib_alt.c -c -fpic -m32
g++ -shared -o metalib_alt.so metalib_alt.o -m32
cd ../..
gcc tmem.c -o tmem.o

#Team 2, Benchmark 1
./RTC/RTC benchmarks/Team2/Bench1/benchmark1.c -rose:output output/Team2/Bench1/rose_benchmark1.c 
g++ -fpermissive benchmarks/Team2/Bench1/benchmark1.c -o2 -o output/Team2/Bench1/benchmark1_original.o
g++ -fpermissive output/Team2/Bench1/rose_benchmark1.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team2/Bench1/benchmark1.o

#Team 2, Benchmark 2
./RTC/RTC benchmarks/Team2/Bench2/benchmark2.c -rose:output output/Team2/Bench2/rose_benchmark2.c 
g++ -fpermissive benchmarks/Team2/Bench2/benchmark2.c -o2 -o output/Team2/Bench2/benchmark2_original.o
g++ -fpermissive output/Team2/Bench2/rose_benchmark2.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team2/Bench2/benchmark2.o

#Team 2, Benchmark 3
./RTC/RTC benchmarks/Team2/Bench3/benchmark3.c -rose:output output/Team2/Bench3/rose_benchmark3.c 
g++ -fpermissive benchmarks/Team2/Bench3/benchmark3.c -o2 -o output/Team2/Bench3/benchmark3_original.o
g++ -fpermissive output/Team2/Bench3/rose_benchmark3.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team2/Bench3/benchmark3.o

#Team 4, Benchmark 1
./RTC/RTC benchmarks/Team4/Bench1/src/buffer-001.c -rose:output output/Team4/Bench1/rose_benchmark1.c 
g++ -fpermissive benchmarks/Team4/Bench1/src/buffer-001.c -o2 -o output/Team4/Bench1/benchmark1_original.o
g++ -fpermissive output/Team4/Bench1/rose_benchmark1.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team4/Bench1/benchmark1.o

#Team 4, Benchmark 2
./RTC/RTC benchmarks/Team4/Bench2/src/free-001.c -rose:output output/Team2/Bench2/rose_benchmark2.c
g++ -fpermissive benchmarks/Team4/Bench2/src/free-001.c -o2 -o output/Team4/Bench2/benchmark2_original.o
g++ -fpermissive output/Team4/Bench2/rose_benchmark2.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team4/Bench2/benchmark2.o

#Team 4, Benchmark 3
#./RTC/RTC benchmarks/Team4/Bench3/src/TheNextPalindrome.c -rose:output output/Team2/Bench3/rose_benchmark3.c
g++ -fpermissive benchmarks/Team4/Bench3/src/TheNextPalindrome.c -o2 -o output/Team4/Bench3/benchmark3_original.o
g++ -fpermissive output/Team4/Bench3/rose_benchmark3.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team4/Bench3/benchmark3.o

#Team 4, Benchmark 4
#g++ -fpermissive benchmarks/Team4/Bench4/src/solitaire.c output/Team4/Bench4/random_lw.c -I output/Team4/Bench4/ -o2 -o output/Team4/Bench4/benchmark4_original.o
#g++ -fpermissive  output/Team4/Bench4/rose_solitaire.c output/Team4/Bench4/random_lw.c -I output/Team4/Bench4/ -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team4/Bench4/benchmark4.o

#Team 4, Benchmark 5
./RTC/RTC benchmarks/Team4/Bench5/src/stdlib-013.c -rose:output output/Team4/Bench5/rose_benchmark5.c
g++ -fpermissive benchmarks/Team4/Bench5/src/stdlib-013.c  -o2 -o output/Team4/Bench5/benchmark5_original.o
g++ -fpermissive output/Team4/Bench5/rose_benchmark5.c -o2 RTC/metadata_libs/metalib_alt.so  RTC/metadata_libs/metadata_alt.so -o output/Team4/Bench5/benchmark5.o